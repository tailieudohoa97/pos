import 'package:flutter/material.dart';
import './TextStyle.dart';

class AddShippingAddress extends StatefulWidget {
  const AddShippingAddress({super.key});

  @override
  State<AddShippingAddress> createState() => _AddShippingAddressState();
}

class _AddShippingAddressState extends State<AddShippingAddress> {
  final TextEditingController _myController = TextEditingController();

  String firstName = "";
  String lastName = "";

  String errorMessageFN = "";
  String errorMessageLN = "";

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // decoration: BoxDecoration(border: Border.all(color: Colors.black)),
        // width: 740,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Title

              // [022312HG] Block, because Don't use Popup layout
              // Container(
              //   margin: const EdgeInsets.only(bottom: 20),
              //   decoration: BoxDecoration(
              //     border: Border(
              //       bottom: BorderSide(
              //         width: 1,
              //         color: Colors.black.withOpacity(0.2),
              //       ),
              //     ),
              //   ),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       const Text(
              //         "Shipping Address",
              //         style: TextStyle(
              //           fontWeight: FontWeight.bold,
              //           color: Colors.black,
              //           fontSize: 15,
              //         ),
              //       ),
              //       CloseButton(
              //         onPressed: () {
              //           Navigator.pop(context);
              //         },
              //       ),
              //     ],
              //   ),
              // ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: FirstNameTextField(
                      onTextFieldChanged: (String value) {
                        setState(() {
                          firstName = value;
                          errorMessageFN = "";
                        });
                      },
                      errorMessage: errorMessageFN,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: LastNameTextField(
                      onTextFieldChanged: (String value) {
                        setState(() {
                          lastName = value;
                          errorMessageLN = "";
                        });
                      },
                      errorMessage: errorMessageLN,
                    ),
                  ),
                ],
              ),
              const PhoneTextField(),
              const AddressTextField(),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Expanded(
                    child: CityTextField(),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: PostCodeTextField(),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Expanded(
                    child: Country(),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: StateTextField(),
                  ),
                ],
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: SizedBox(
                    height: 40,
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        if (firstName == "") {
                          setState(() {
                            errorMessageFN = "This field is required";
                          });
                        } else if (lastName == "") {
                          setState(() {
                            errorMessageLN = "This field is required";
                          });
                        } else {
                          setState(() {
                            errorMessageFN = "";
                            errorMessageLN = "";
                          });
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(9, 173, 170, 1),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                            color: Color.fromRGBO(9, 173, 170, 1),
                            width: 0.2,
                          ),
                          borderRadius: BorderRadius.circular(0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Save Shipping Address'.toUpperCase(),
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

class FirstNameTextField extends StatelessWidget {
  final ValueChanged<String> onTextFieldChanged;
  String errorMessage = "";
  FirstNameTextField(
      {required this.onTextFieldChanged, required this.errorMessage});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "First Name *",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (String value) {
              onTextFieldChanged(value);
            },
            decoration: const InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
        if (errorMessage != "")
          Text(
            errorMessage,
            style: const TextStyle(color: Colors.red),
          )
      ],
    );
  }
}

class LastNameTextField extends StatelessWidget {
  final ValueChanged<String> onTextFieldChanged;
  String errorMessage = "";
  LastNameTextField(
      {super.key,
      required this.onTextFieldChanged,
      required this.errorMessage});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Last name *",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (String value) {
              onTextFieldChanged(value);
            },
            decoration: const InputDecoration(
                // errorText: 'This field is required',
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
        if (errorMessage != "")
          Text(
            errorMessage,
            style: const TextStyle(color: Colors.red),
          )
      ],
    );
  }
}

class PhoneTextField extends StatelessWidget {
  const PhoneTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Phone",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            decoration: InputDecoration(
                // errorText: 'This field is required',
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class AddressTextField extends StatelessWidget {
  const AddressTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Address",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            decoration: InputDecoration(
                // errorText: 'This field is required',
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class CityTextField extends StatelessWidget {
  const CityTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "City",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class PostCodeTextField extends StatelessWidget {
  const PostCodeTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Post Code",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class Country extends StatefulWidget {
  const Country({super.key});

  @override
  State<Country> createState() => _Country();
}

class _Country extends State<Country> {
  String? shippingMethod;

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = ['Hà Nội', 'Hồ Chí Minh'];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Country",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                //Ban đầu rỗng, sẽ hiển thị cái hint
                                hint: const Text('Choose a Country'),
                                value: shippingMethod,
                                isExpanded: true,
                                underline: const SizedBox(),
                                style: const TextStyle(
                                  fontSize: 13,
                                  color: Colors.black,
                                ),
                                icon: const Icon(Icons.keyboard_arrow_down),
                                // Mỗi lần click chọn 1 item
                                // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                                // dẫn tới "value: chooseStore," ở phía trên thay đổi
                                // Và cái hint sẽ bị thay = cái string item đã chọn
                                onChanged: (newValue) {
                                  setState(() {
                                    shippingMethod = newValue
                                        .toString(); //Phải cùng kiểu String >>> .toString()
                                  });
                                },

                                //Tạo list item, layout cho 1 item
                                items: listItem.map((newValue) {
                                  return DropdownMenuItem(
                                    // alignment: Alignment.centerLeft,
                                    value: newValue,
                                    child: Text(newValue),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class StateTextField extends StatelessWidget {
  const StateTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "State",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}
