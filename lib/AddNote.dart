import 'package:flutter/material.dart';
import './TextStyle.dart';

class AddNote extends StatefulWidget {
  final double order_total;

  AddNote({
    super.key,
    required this.order_total,
  });

  @override
  State<AddNote> createState() => _AddNoteState();
}

class _AddNoteState extends State<AddNote> {
  bool hasText = false;
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
        width: 540,
        child: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Close button
              Container(
                alignment: Alignment.topRight,
                child: CloseButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),

              //Title
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                padding: const EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.black.withOpacity(0.2),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Order total",
                      style: TextStyleGlobal.titlePopupBlackTextStyle,
                    ),
                    Text(
                      "\$${widget.order_total}",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(9, 173, 170, 1),
                        fontSize: 18,
                      ),
                    )
                  ],
                ),
              ),

              NoteTextField(
                  controller: controller,
                  handleOnChange: (value) {
                    setState(() {
                      if (controller.text.isNotEmpty) {
                        hasText = true;
                      } else {
                        hasText = false;
                      }
                    });
                  }),
              SaveNoteButton(
                isActive: hasText,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class NoteTextField extends StatelessWidget {
  final Function handleOnChange;
  final TextEditingController controller;
  NoteTextField(
      {super.key, required this.handleOnChange, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Add a note to this order",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),

        //Note
        Stack(
          children: [
            TextField(
              onChanged: (value) => handleOnChange(value),
              controller: controller,
              maxLines: 8,
              minLines: 5,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xFFdbdbdb),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xFF09ADAA),
                  ),
                ),
                contentPadding: EdgeInsets.fromLTRB(50, 15, 15, 15),
              ),
              style: const TextStyle(
                fontSize: 14,
              ),
            ),
            const Positioned(
              top: 10,
              left: 10,
              child: Icon(Icons.message),
            ),
          ],
        ),
      ],
    );
  }
}

class SaveNoteButton extends StatelessWidget {
  final bool isActive;
  const SaveNoteButton({super.key, required this.isActive});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 0),
      padding: const EdgeInsets.only(top: 20),
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(
            child: ElevatedButton(
              onPressed: () {
                // ignore: avoid_print
                isActive ? print("Saved Note") : {};
              },
              style: ButtonStyle(
                shape: MaterialStatePropertyAll(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    // side: const BorderSide(color: Colors.red),
                  ),
                ),
                foregroundColor: const MaterialStatePropertyAll(
                  Colors.white,
                ),
                padding: const MaterialStatePropertyAll(
                  EdgeInsets.all(20),
                ),
                backgroundColor: MaterialStatePropertyAll(isActive
                    ? const Color.fromRGBO(9, 173, 170, 1)
                    : const Color.fromRGBO(77, 77, 77, 1)),
              ),
              child: Text(
                'Save note'.toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
