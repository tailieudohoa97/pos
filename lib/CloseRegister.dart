import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';

class CloseRegister extends StatelessWidget {
  const CloseRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: Colors.black)),
      width: 540,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Close Register",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 15)),
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.close))
                ],
              ),
            ),
            Divider(
              height: 10,
              thickness: 2,
              color: Colors.black.withOpacity(0.2),
            ),
            const SizedBox(
              height: 16,
            ),
            const OpeningTime(),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black.withOpacity(0.2),
            ),
            const ClosingTime(),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black.withOpacity(0.2),
            ),
            const Cashier(),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black.withOpacity(0.2),
            ),
            const ProfitItem(
              data: '6',
              title: 'Orders',
            ),
            const ProfitItem(
              data: '13',
              title: 'Products',
            ),
            const ProfitItem(
              data: '\$800',
              title: 'Cash in hand',
            ),
            const ProfitItem(
              data: '\$98.71',
              title: 'Check payments',
            ),
            const ProfitItem(
              data: '\$213.34',
              title: 'Cash',
            ),
            const ProfitItem(
              data: '\$123.3',
              title: 'Chip and Pin',
            ),
            const ProfitItem(
              data: '\$234.2',
              title: 'Net Sales',
            ),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black.withOpacity(0.2),
            ),
            const TotalSales(),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black.withOpacity(0.2),
            ),
            const CashTotal(),
            Divider(
              height: 10,
              thickness: 1,
              color: Colors.black.withOpacity(0.2),
            ),
            const NoteTextField(),
            const SizedBox(
              height: 16,
            ),
            const DownloadReportButton(),
            const SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }
}

class OpeningTime extends StatelessWidget {
  const OpeningTime({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text("Opening Time",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15)),
          Text(DateTime.now().toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                  fontSize: 15))
        ],
      ),
    );
  }
}

class ClosingTime extends StatelessWidget {
  const ClosingTime({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text("Closing Time",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15)),
          Text(DateTime.now().toString(),
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                  fontSize: 15))
        ],
      ),
    );
  }
}

class NoteTextField extends StatelessWidget {
  const NoteTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text("Add a note (optinal)",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 12)),
        ),
        TextField(
          maxLines: 3,
          minLines: 2,
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.note_sharp),
            border:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          ),
        ),
      ],
    );
  }
}

class DownloadReportButton extends StatelessWidget {
  const DownloadReportButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        ElevatedButton(
          onPressed: () {
            // ignore: avoid_print
          },
          style: ButtonStyle(
            shape: MaterialStatePropertyAll(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
                // side: const BorderSide(color: Colors.red),
              ),
            ),
            foregroundColor: const MaterialStatePropertyAll(
              Colors.white,
            ),
            padding: const MaterialStatePropertyAll(
              EdgeInsets.all(20),
            ),
            backgroundColor:
                const MaterialStatePropertyAll(Color.fromRGBO(77, 77, 77, 1)),
          ),
          child: Text(
            'Download Report'.toUpperCase(),
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}

class TotalSales extends StatelessWidget {
  const TotalSales({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Text("Total Sales",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(9, 173, 170, 1),
                  fontSize: 18)),
          Text("\$720.34",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(9, 173, 170, 1),
                  fontSize: 18))
        ],
      ),
    );
  }
}

class CashTotal extends StatelessWidget {
  const CashTotal({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Text("Cash Total",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(160, 167, 0, 1),
                  fontSize: 18)),
          Text("\$720.34",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(160, 167, 0, 1),
                  fontSize: 18))
        ],
      ),
    );
  }
}

class Cashier extends StatelessWidget {
  const Cashier({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Text("Cashiers",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15)),
          Text("Hugo",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                  fontSize: 15))
        ],
      ),
    );
  }
}

class ProfitItem extends StatelessWidget {
  final String title;
  final String data;
  const ProfitItem({super.key, required this.title, required this.data});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15)),
          Text(data,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15))
        ],
      ),
    );
  }
}
