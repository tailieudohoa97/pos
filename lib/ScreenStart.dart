// ignore_for_file: file_names
import 'package:flutter/material.dart';
import 'package:my_pos_122219/RegisterMain.dart';
import 'package:my_pos_122219/main.dart';
import './TextStyle.dart';

//Screen Start
class ScreenStart extends StatelessWidget {
  const ScreenStart({super.key});

  @override
  Widget build(BuildContext context) {
    disableBrowserBackButton();
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    // .........
    // KHUNG LỚN NHẤT
    // .........
    return Scaffold(
      body: Container(
        width: screenWidth,
        height: screenHeight,
        padding: const EdgeInsets.all(50.0),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/BG-main.png'),
            fit: BoxFit.cover,
          ),
        ),
        alignment: Alignment.center,
        // .........
        // KHUNG NHỎ
        // .........
        child: ConstrainedBox(
          constraints: const BoxConstraints(
            minWidth: 400,
            maxWidth: 450,
          ),
          child: Container(
            // width: 450.0,
            padding: const EdgeInsets.all(50.0),
            decoration: const BoxDecoration(color: Colors.white),
            clipBehavior: Clip.hardEdge,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //Logo
                  Image.network(
                    'https://docs.flutter.dev/assets/images/shared/brand/flutter/logo/flutter-lockup.png',
                    width: 180,
                  ),
                  const SizedBox(height: 20),

                  //Choose store and register Layout
                  //Title Page
                  const Text(
                    'Choose Store and register',
                    // style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    style: TextStyleGlobal.xlgBlackTextStyle,
                  ),
                  const SizedBox(height: 20),

                  //Choose Store
                  const Choose_Store(),
                  const SizedBox(
                    height: 20,
                  ),

                  //Choose Register
                  const Choose_Register(),
                  const SizedBox(height: 20),

                  //Button Open Register
                  const ButtonOpenRegister(),
                  const SizedBox(height: 20),

                  //Button Logout
                  const ButtonLogout(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//CHOOSE STORE
// ignore: camel_case_types
class Choose_Store extends StatefulWidget {
  const Choose_Store({super.key});

  @override
  State<Choose_Store> createState() => _Choose_StoreState();
}

// ignore: camel_case_types
class _Choose_StoreState extends State<Choose_Store> {
  // Phải có giá trị đầu tiên và phải nằm trong list
  String chooseStore = 'Choose a Store';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;
  List listItem = [
    'Choose a Store',
    'Vietnam',
    'Laos',
    'Campuchia',
    'Thailand'
  ];

  @override
  Widget build(BuildContext context) {
    var labelStyle = const TextStyle(
      color: Color.fromRGBO(9, 173, 170, 1),
      fontSize: 10,
    );

    return Container(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          // color: const Color.fromRGBO(9, 173, 170, 1),
          color: const Color.fromRGBO(234, 234, 234, 1),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: Stack(
              children: [
                //Label
                Positioned(
                  child: Text(
                    'Store',
                    style: labelStyle,
                    textAlign: TextAlign.left,
                  ),
                ),

                Row(
                  children: [
                    Expanded(
                      child: DropdownButton(
                        //Ban đầu rỗng, sẽ hiển thị cái hint
                        hint: const Text('Choose a Store'),
                        value: chooseStore,
                        isExpanded: true,
                        underline: const SizedBox(),
                        style: const TextStyle(
                          fontSize: 13,
                          color: Colors.black,
                        ),
                        icon: const Icon(Icons.keyboard_arrow_down),
                        // Mỗi lần click chọn 1 item
                        // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                        // dẫn tới "value: chooseStore," ở phía trên thay đổi
                        // Và cái hint sẽ bị thay = cái string item đã chọn
                        onChanged: (newValue) {
                          setState(() {
                            chooseStore = newValue
                                .toString(); //Phải cùng kiểu String >>> .toString()
                          });
                        },

                        //Tạo list item, layout cho 1 item
                        items: listItem.map((newValue) {
                          return DropdownMenuItem(
                            // alignment: Alignment.centerLeft,
                            value: newValue,
                            child: Text(newValue),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

//CHOOSE REGISTER
// ignore: camel_case_types
class Choose_Register extends StatefulWidget {
  const Choose_Register({super.key});

  @override
  State<Choose_Register> createState() => _Choose_RegisterState();
}

// ignore: camel_case_types
class _Choose_RegisterState extends State<Choose_Register> {
  String chooseRegister = 'Choose a Register';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseRegister;
  List listItem = [
    'Choose a Register',
    'Register 1',
    'Register 2',
    'Register 3'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 45,
      // padding: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          // color: const Color.fromRGBO(9, 173, 170, 1),
          color: const Color.fromRGBO(234, 234, 234, 1),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Stack(
              children: [
                const Positioned(
                  child: Text(
                    'Register',
                    style: TextStyle(
                      color: Color.fromRGBO(9, 173, 170, 1),
                      fontSize: 10,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          // alignedDropdown: true là chiều rộng của menu DropdownButton sẽ khớp với chiều rộng của nút.
                          // alignedDropdown: true,
                          child: DropdownButton(
                            //Ban đầu rỗng, sẽ hiển thị cái hint

                            isExpanded: true,
                            underline: const SizedBox(),
                            style: const TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                            ),
                            icon: const Icon(Icons.keyboard_arrow_down),
                            hint: const Text('Choose a Store'),
                            value: chooseRegister,
                            // Mỗi lần click chọn 1 item
                            // thì hàm Onchange sẽ sét lại value mới = value mới vừa chọn
                            // dẫn tới "value: chooseRegister," ở phía trên thay đổi
                            // Và cái hint sẽ bị thay = cái string item đã chọn
                            onChanged: (newValue) {
                              setState(() {
                                chooseRegister = newValue
                                    .toString(); //Phải cùng kiểu String >>> .toString()
                              });
                            },

                            //Tạo list item, layout cho 1 item
                            items: listItem.map((newValue) {
                              return DropdownMenuItem(
                                value: newValue,
                                child: Text(newValue),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

//Button Open Register
class ButtonOpenRegister extends StatelessWidget {
  const ButtonOpenRegister({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              // ignore: avoid_print
              Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (context, animation, secondaryAnimation) =>
                      const RegisterScreen(),
                  transitionsBuilder:
                      (context, animation, secondaryAnimation, child) {
                    return FadeTransition(
                      opacity: animation,
                      child: child,
                    );
                  },
                ),
              );
            },
            style: ButtonStyle(
              shape: MaterialStatePropertyAll(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0.0),
                  // side: const BorderSide(color: Colors.red),
                ),
              ),
              foregroundColor: const MaterialStatePropertyAll(
                Colors.white,
              ),
              padding: const MaterialStatePropertyAll(
                EdgeInsets.all(20),
              ),
              backgroundColor: const MaterialStatePropertyAll(
                Color.fromRGBO(9, 173, 170, 1),
              ),
            ),
            child: Text(
              "Open Register".toUpperCase(),
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

//Button Logout
class ButtonLogout extends StatelessWidget {
  const ButtonLogout({super.key});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        // ignore: avoid_print
        print('Logout');
      },
      style: TextButton.styleFrom(
        foregroundColor: const Color.fromRGBO(176, 5, 5, 1),
      ),
      child: const Text(
        'Logout',
        style: TextStyle(
          decoration: TextDecoration.underline,
          fontSize: 13,
        ),
      ),
    );
  }
}
