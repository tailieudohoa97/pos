// ignore_for_file: file_names, avoid_print, duplicate_ignore
import 'package:flutter/material.dart';
import 'package:my_pos_122219/RegisterMain.dart';
import 'package:my_pos_122219/ScreenStart.dart';
import './TextStyle.dart';

//Screen Start
class ScreenLogin extends StatelessWidget {
  const ScreenLogin({super.key});

  @override
  Widget build(BuildContext context) {
    disableBrowserBackButton();
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    // const String titlePage = 'Choose Store and register';
    // .........
    // KHUNG LỚN NHẤT
    // .........
    return Container(
      width: screenWidth,
      height: screenHeight,
      padding: const EdgeInsets.all(50.0),
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/BG-main.png'),
          fit: BoxFit.cover,
        ),
      ),
      alignment: Alignment.center,
      // .........
      // KHUNG NHỎ
      // .........
      child: ConstrainedBox(
        constraints: const BoxConstraints(
          minWidth: 400,
          maxWidth: 450,
        ),
        child: Container(
          // width: 450.0,
          padding: const EdgeInsets.all(50.0),
          decoration: const BoxDecoration(color: Colors.white),
          clipBehavior: Clip.hardEdge,
          child: SingleChildScrollView(
            child: Column(
              children: [
                //Logo
                Image.network(
                  'https://docs.flutter.dev/assets/images/shared/brand/flutter/logo/flutter-lockup.png',
                  width: 180,
                ),
                const SizedBox(height: 20),

                //Title Page
                const Text(
                  'Login',
                  // style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  style: TextStyleGlobal.xlgBlackTextStyle,
                ),
                const SizedBox(height: 20),

                const UserEmail(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class UserEmail extends StatefulWidget {
  const UserEmail({super.key});

  @override
  State<UserEmail> createState() => _UserEmailState();
}

class _UserEmailState extends State<UserEmail> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _isChecked = false;
  // String username = "";
// String
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(
          children: [
            //USER
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                // keyboardType: TextInputType.text,
                // onChanged: (newUsername) {
                //   print(newUsername);
                //   username = newUsername;
                // },

                //Đổi màu con trỏ

                cursorColor: const Color.fromRGBO(9, 173, 170, 1),
                style: const TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
                controller: nameController, //Tên hàm
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),

                  //
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(234, 234, 234, 1),
                    ),
                  ),

                  //
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(9, 173, 170, 1),
                    ),
                  ),

                  // errorText: 'This is an error TextField',
                  labelText: 'Username or email address *',
                  labelStyle: TextStyleGlobal.formTextStyle,
                  // hintText: 'Username or email address *',
                  // hintStyle: TextStyle(
                  //   color: Color.fromARGB(255, 110, 110, 110),
                  // ),
                ),
              ),
            ),

            //PASSWORD
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                //Đổi màu con trỏ
                cursorColor: const Color.fromRGBO(9, 173, 170, 1),

                obscureText: true, //Pass thành dấu ****
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),

                  //
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(234, 234, 234, 1),
                    ),
                  ),

                  //
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(9, 173, 170, 1),
                    ),
                  ),

                  //
                  labelText: 'Password *',
                  labelStyle: TextStyleGlobal.formTextStyle,
                ),
              ),
            ),

            CheckboxListTile(
              value: _isChecked,
              onChanged: (newIsChecked) {
                setState(() {
                  _isChecked = newIsChecked as bool;
                });
              },

              //Chuyển checkbox ra trước
              controlAffinity: ListTileControlAffinity.leading,

              contentPadding: const EdgeInsets.only(left: 5, top: 0),
              checkColor: const Color.fromARGB(255, 255, 255, 255),
              activeColor: const Color.fromRGBO(9, 173, 170, 1),
              // checkboxShape: OutlinedBorder(
              //   side: BorderSide(
              //     width: 3,
              //     color: Color.fromRGBO(234, 234, 234, 1),
              //   ),
              // ),
              title: const Text(
                "Remember Me",
                style: TextStyle(fontSize: 13),
              ),
              // subtitle: const Text(
              //   'sub Title',
              //   style: TextStyle(fontSize: 11),
              // ),
              tileColor: const Color.fromARGB(31, 238, 1, 1),
            ),
            //FORGOT PASSWORD
            // TextButton(
            //   onPressed: () {
            //     //forgot password screen
            //   },
            //   child: const Text(
            //     'Forgot Password',
            //   ),
            // ),

            //LOGIN BUTTON
            Container(
              // height: 50,
              // decoration: BoxDecoration(
              //   border: Border.all(
              //     width: 1,
              //     color: const Color.fromRGBO(9, 173, 170, 1),
              //   ),
              // ),
              margin: const EdgeInsets.only(top: 0),
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              alignment: Alignment.center,
              child: Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        // ignore: avoid_print
                        Navigator.push(
                          context,
                          PageRouteBuilder(
                            pageBuilder:
                                (context, animation, secondaryAnimation) =>
                                    const ScreenStart(),
                            transitionsBuilder: (context, animation,
                                secondaryAnimation, child) {
                              return FadeTransition(
                                opacity: animation,
                                child: child,
                              );
                            },
                          ),
                        );
                      },
                      style: ButtonStyle(
                        shape: MaterialStatePropertyAll(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            // side: const BorderSide(color: Colors.red),
                          ),
                        ),
                        foregroundColor: const MaterialStatePropertyAll(
                          Colors.white,
                        ),
                        padding: const MaterialStatePropertyAll(
                          EdgeInsets.all(20),
                        ),
                        backgroundColor: const MaterialStatePropertyAll(
                          Color.fromRGBO(9, 173, 170, 1),
                        ),
                      ),
                      child: Text(
                        'Log in'.toUpperCase(),
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),

            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     const Text('Does not have account?'),
            //     TextButton(
            //       child: const Text(
            //         'Sign in',
            //         style: TextStyle(fontSize: 20),
            //       ),
            //       onPressed: () {
            //         //signup screen
            //       },
            //     )
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
