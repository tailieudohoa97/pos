import 'package:flutter/material.dart';

//Edit button
class EditButton extends StatefulWidget {
  const EditButton({super.key});

  @override
  State<EditButton> createState() => _EditButtonState();
}

class _EditButtonState extends State<EditButton> {
  bool hover = true;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 50,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        onHover: (event) {
          setState(() {
            hover = true;
          });
        },
        onExit: ((event) {
          setState(() {
            hover = false;
          });
        }),
        child: ElevatedButton(
          onPressed: () {},
          style: TextButton.styleFrom(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            padding: const EdgeInsets.all(0),
            foregroundColor: (hover == false)
                ? const Color(0x59000000)
                : const Color(0xFF000000),
            backgroundColor: const Color(0x0009ADAA),
          ),
          child: const Icon(
            Icons.edit,
            size: 15,
          ),
        ),
      ),
    );
  }
}
