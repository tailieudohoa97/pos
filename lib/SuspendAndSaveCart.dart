import 'package:flutter/material.dart';

class SuspendAndSaveCart extends StatelessWidget {
  const SuspendAndSaveCart({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: Colors.black)),
      height: 340,
      width: 540,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Suspend and save cart",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 15)),
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.close))
                ],
              ),
            ),
            Divider(
              height: 10,
              thickness: 2,
              color: Colors.black.withOpacity(0.2),
            ),
            const SizedBox(
              height: 16,
            ),
            const NoteTextField(),
            const SaveNoteButton()
          ],
        ),
      ),
    );
  }
}

class NoteTextField extends StatelessWidget {
  const NoteTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text("Add a reason or note",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 12)),
        ),
        TextField(
          maxLines: 4,
          minLines: 3,
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.note_sharp),
            border:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          ),
        ),
      ],
    );
  }
}

class SaveNoteButton extends StatelessWidget {
  const SaveNoteButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 0),
      padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(
            child: ElevatedButton(
              onPressed: () {
                // ignore: avoid_print
              },
              style: ButtonStyle(
                shape: MaterialStatePropertyAll(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                    // side: const BorderSide(color: Colors.red),
                  ),
                ),
                foregroundColor: const MaterialStatePropertyAll(
                  Colors.white,
                ),
                padding: const MaterialStatePropertyAll(
                  EdgeInsets.all(20),
                ),
                backgroundColor: const MaterialStatePropertyAll(
                    Color.fromRGBO(224, 153, 20, 1)),
              ),
              child: Text(
                'SUSPEND AND SAVE CART'.toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
