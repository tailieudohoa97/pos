import 'dart:convert';
//import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:my_pos_122219/PrintReceipt.dart';

import 'Numpad.dart';

class Payment extends StatefulWidget {
  final double orderAmount;
  const Payment({super.key, required this.orderAmount});

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  final TextEditingController _myController = TextEditingController();

  final double size = 770;
  void initState() {
    super.initState();
    for (TextEditingController controller in Temp_controllers) {
      controller.addListener(_onTextChanged);
    }
  }

  void _onTextChanged() {
    // Handle the text change event here
    setState(() {
      for (TextEditingController controller in Temp_controllers) {
        if (controller.text.isEmpty) {
          _getSum();
        } else {
          _getSum();
        }
      }

      //print(ShippingFee);
    });
  }

  List<Widget> Option = [];
  // Create an array store 6 payment option controller
  List<TextEditingController> Temp_controllers =
      List.generate(6, (index) => TextEditingController());
  // Function to change controller when click on each controller
  TextEditingController ChangeController(int index) {
    _getSum();
    return Temp_controllers[index];
  }

  double _getSum() {
    double sum = 0.0;
    for (TextEditingController controller in Temp_controllers) {
      if (controller.text != "") {
        sum += double.parse(controller.text);
      }
    }
    return sum;
  }

  double balance = 0;
  double change = 0;
  int index = 0;
  @override
  Widget build(BuildContext context) {
    balance = _getSum() < widget.orderAmount
        ? double.parse((widget.orderAmount - _getSum()).toStringAsFixed(2))
        : 0;
    change = _getSum() > balance
        ? double.parse((_getSum() - widget.orderAmount).toStringAsFixed(2))
        : 0;
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.black)),
        width: size,
        child: IntrinsicHeight(
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    TotalPaying(totalPaying: _getSum()),
                    const SizedBox(
                      height: 1,
                    ),
                    Balance(balance: balance),
                    const SizedBox(
                      height: 1,
                    ),
                    Change(change: change),
                    const SizedBox(
                      height: 1,
                    ),
                    Expanded(
                      child: Container(
                        color: const Color.fromRGBO(234, 234, 234, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 540,
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                          alignment: const Alignment(1, -1),
                          child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(Icons.close))),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Split payment",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 18)),
                            Text("\$${widget.orderAmount}",
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromRGBO(9, 173, 170, 1),
                                    fontSize: 18))
                          ],
                        ),
                      ),
                      Divider(
                        height: 10,
                        thickness: 2,
                        color: Colors.black.withOpacity(0.2),
                      ),
                      PaymentOption(
                        id: "0",
                        controller: Temp_controllers[0],
                        onTap: (id) {
                          setState(() {
                            index = int.parse(id);
                          });
                        },
                      ),
                      ...Option,
                      AddPaymentOptionButton(handleOnChange: () {
                        setState(() {
                          index += 1;
                          Option.add(PaymentOption(
                            id: (index).toString(),
                            controller: Temp_controllers[index],
                            onTap: (id) {
                              setState(() {
                                index = int.parse(id);
                              });
                            },
                          ));
                        });
                      }),
                      const SizedBox(
                        height: 16,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Text("Popular tendered",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 12)),
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: PopularTendered(
                              data: "10.20",
                              handleOnChange: (value) {
                                print("Hiiii");
                              },
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: PopularTendered(
                              data: "10.25",
                              handleOnChange: () {},
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: PopularTendered(
                              data: "11.00",
                              handleOnChange: () {},
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: PopularTendered(
                              data: "15.00",
                              handleOnChange: () {},
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: PopularTendered(
                              data: "20.00",
                              handleOnChange: () {},
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: PopularTendered(
                              data: "50.00",
                              handleOnChange: () {},
                            ),
                          )),
                        ],
                      ),
                      NumPad(
                        submitText: "PAY",
                        buttonSize: 75,
                        buttonColor: Colors.white,
                        iconColor: const Color.fromRGBO(9, 173, 170, 1),
                        controller: ChangeController(index),
                        delete: () {
                          ChangeController(index).text = ChangeController(index)
                              .text
                              .substring(
                                  0, ChangeController(index).text.length - 1);
                        },
                        clear: () {
                          ChangeController(index).text = "";
                        },
                        // do something with the input numbers
                        onSubmit: () {
                          Navigator.pop(context, false);
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                    content: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black)),
                                        width: 740,
                                        child: const PrintReceipt()));
                              });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AddPaymentOptionButton extends StatelessWidget {
  final Function handleOnChange;

  const AddPaymentOptionButton({super.key, required this.handleOnChange});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: SizedBox(
          height: 40,
          width: double.infinity,
          child: ElevatedButton(
            onPressed: () => handleOnChange(),
            style: ElevatedButton.styleFrom(
              backgroundColor: const Color.fromRGBO(9, 173, 170, 1),
              shape: RoundedRectangleBorder(
                side: const BorderSide(
                    color: Color.fromRGBO(9, 173, 170, 1), width: 0.2),
                borderRadius: BorderRadius.circular(0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(Icons.add),
                Text(
                  'Add another payment option'.toUpperCase(),
                ),
              ],
            ),
          )),
    );
  }
}

class PaymentOption extends StatelessWidget {
  final String id;
  final TextEditingController controller;
  final Function onTap;
  const PaymentOption({
    super.key,
    required this.controller,
    required this.onTap,
    required this.id,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AmountTextField(
          onTap: () => onTap(id),
          controller: controller,
        ),
        const PayMethod(),
      ],
    );
  }
}

class GreyBg extends StatelessWidget {
  const GreyBg({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(234, 234, 234, 1),
    );
  }
}

class PopularTendered extends StatefulWidget {
  final Function handleOnChange;
  final String data;
  const PopularTendered(
      {super.key, required this.handleOnChange, required this.data});
  @override
  State<PopularTendered> createState() => _PopularTenderedState();
}

class _PopularTenderedState extends State<PopularTendered> {
  Color _color = Colors.grey;

  bool isHover = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 40,
        width: 100,
        child: ElevatedButton(
            onHover: (event) => {
                  setState(
                    () {
                      isHover = !isHover;
                    },
                  )
                },
            onPressed: () => widget.handleOnChange(widget.data),
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: isHover
                        ? const Color.fromRGBO(9, 173, 170, 1)
                        : Colors.black,
                    width: 0.2),
                borderRadius: BorderRadius.circular(0),
              ),
            ),
            child: Text(
              "\$${widget.data}",
              style: TextStyle(
                  fontSize: 12,
                  fontWeight: isHover ? FontWeight.bold : FontWeight.normal,
                  color: isHover
                      ? const Color.fromRGBO(9, 173, 170, 1)
                      : Colors.grey),
            )));
  }
}

class TotalPaying extends StatelessWidget {
  final double totalPaying;
  const TotalPaying({super.key, required this.totalPaying});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(234, 234, 234, 1),
      height: 70,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [
            const Align(
              alignment: Alignment(1, 1),
              child: Text("Total Paying",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: Colors.black,
                      fontSize: 15,
                      height: 2)),
            ),
            const SizedBox(
              height: 10,
            ),
            Align(
              alignment: const Alignment(1, 1),
              child: Text("\$$totalPaying",
                  style: const TextStyle(
                      fontWeight: FontWeight.w900,
                      color: Colors.black,
                      fontSize: 20)),
            ),
          ],
        ),
      ),
    );
  }
}

class Balance extends StatelessWidget {
  final double balance;
  const Balance({super.key, required this.balance});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(234, 234, 234, 1),
      height: 70,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [
            const Align(
              alignment: Alignment(1, 1),
              child: Text("Balance",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: Colors.black,
                      fontSize: 15,
                      height: 2)),
            ),
            const SizedBox(
              height: 10,
            ),
            Align(
              alignment: const Alignment(1, 1),
              child: Text("\$$balance",
                  style: const TextStyle(
                      fontWeight: FontWeight.w900,
                      color: Colors.red,
                      fontSize: 20)),
            ),
          ],
        ),
      ),
    );
  }
}

class Change extends StatelessWidget {
  final double change;
  const Change({super.key, required this.change});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(234, 234, 234, 1),
      height: 70,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [
            const Align(
              alignment: Alignment(1, 1),
              child: Text("Change",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: Colors.black,
                      fontSize: 15,
                      height: 2)),
            ),
            const SizedBox(
              height: 10,
            ),
            Align(
              alignment: const Alignment(1, 1),
              child: Text("\$$change",
                  style: const TextStyle(
                      fontWeight: FontWeight.w900,
                      color: Colors.black,
                      fontSize: 20)),
            ),
          ],
        ),
      ),
    );
  }
}

class AmountTextField extends StatelessWidget {
  final TextEditingController controller;
  final Function onTap;
  const AmountTextField({
    Key? key,
    required this.controller,
    required this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: SizedBox(
        width: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text("Amount",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 10)),
            ),
            TextField(
              onTap: () => onTap(),
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
              ],
              controller: controller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(9, 173, 170, 1))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PayMethod extends StatefulWidget {
  const PayMethod({super.key});

  @override
  State<PayMethod> createState() => _PayMethodState();
}

class _PayMethodState extends State<PayMethod> {
  String shippingMethod = 'Cash';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = [
    'Direct bank transfer',
    'Check payments',
    'Cash on delivery',
    'PayPal',
    'Cash',
    'Chip and Pin'
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: Text("Amount paying",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 10)),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
            width: 200,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                // color: const Color.fromRGBO(9, 173, 170, 1),
                color: const Color.fromRGBO(234, 234, 234, 1),
              ),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Icon(Icons.qr_code_scanner),
                                  SizedBox(width: 5),
                                  Text('Cash'),
                                ],
                              ),
                              value: shippingMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                setState(() {
                                  shippingMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
