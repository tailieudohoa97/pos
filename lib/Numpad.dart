import 'package:flutter/material.dart';

class NumPad extends StatelessWidget {
  final double buttonSize;
  final Color buttonColor;
  final Color iconColor;
  final TextEditingController controller;
  final Function delete;
  final Function onSubmit;
  final Function clear;
  final String submitText;

  const NumPad({
    Key? key,
    this.buttonSize = 100,
    this.buttonColor = Colors.white,
    this.iconColor = Colors.amber,
    required this.delete,
    required this.clear,
    required this.onSubmit,
    required this.controller,
    required this.submitText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: const Color(0xFFdbdbdb),
          width: 1,
        ),
      ),
      clipBehavior: Clip.hardEdge,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // implement the number keys (from 0 to 9) with the NumberButton widget
            // the NumberButton widget is defined in the bottom of this file
            children: [
              NumberButton(
                number: 1,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: 2,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: 3,
                color: buttonColor,
                controller: controller,
              ),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: 60,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      elevation: 0,
                      foregroundColor: Colors.black26,
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                            color: Color(0xFFdbdbdb), width: 0.2),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                    onPressed: () => delete(), // Gọi hàm delete khi bấm nút này
                    child: const Center(child: Icon(Icons.arrow_back)),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              NumberButton(
                number: 4,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: 5,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: 6,
                color: buttonColor,
                controller: controller,
              ),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: 60,
                  child: TextButton(
                    onPressed: () => clear(),
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.white,
                      elevation: 0,
                      foregroundColor: Colors.black26,
                      textStyle: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                          color: Color(0xFFdbdbdb),
                          width: 0.2,
                        ),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                    child: const Text('Clear'),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              NumberButton(
                number: 7,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: 8,
                color: buttonColor,
                controller: controller,
              ),
              NumberButton(
                number: 9,
                color: buttonColor,
                controller: controller,
              ),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: 60,
                  child: TextButton(
                    onPressed: () => onSubmit(),
                    style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFF09ADAA),
                      elevation: 0,
                      foregroundColor: Colors.white,
                      textStyle: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                          color: Color(0xFF09ADAA),
                          width: 0.2,
                        ),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                    child: Text(submitText),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              // this button is used to delete the last number

              NumberButton(
                number: 0,
                color: buttonColor,
                controller: controller,
              ),
              // this button is used to submit the entered value
              NumberDot(title: ".", controller: controller),
              NumberDot(title: "00", controller: controller),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: 60,
                  child: TextButton(
                    // onPressed: () => onSubmit(),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFFE7E7E7),
                      elevation: 0,
                      foregroundColor: Colors.grey,
                      textStyle: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                          color: Color(0xFFdbdbdb),
                          width: 0.2,
                        ),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                    child: const Text("BACK"),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

// define NumberButton widget
// its shape is round
class NumberButton extends StatelessWidget {
  final int number;

  final Color color;
  final TextEditingController controller;

  const NumberButton({
    Key? key,
    required this.number,
    required this.color,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: SizedBox(
        height: 60,
        // width: 100,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: color,
            shape: RoundedRectangleBorder(
              side: const BorderSide(color: Color(0xFFdbdbdb), width: 0.2),
              borderRadius: BorderRadius.circular(0),
            ),
          ),
          onPressed: () {
            controller.text += number
                .toString(); // controller trỏ vào element của widget sẽ tự động được cộng thêm giá trị của số vào sau khi nhấn
          },
          child: Center(
            child: Text(
              number.toString(),
              style: const TextStyle(
                color: Colors.black,
                fontSize: 18,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// Phím dấu .
class NumberDot extends StatefulWidget {
  final String title;
  final TextEditingController controller;
  NumberDot({
    super.key,
    required this.title,
    required this.controller,
  });

  @override
  State<NumberDot> createState() => _NumberDotState();
}

class _NumberDotState extends State<NumberDot> {
  bool hasDot = false;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: SizedBox(
        height: 60,
        // width: 100,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              side: const BorderSide(color: Color(0xFFdbdbdb), width: 0.2),
              borderRadius: BorderRadius.circular(0),
            ),
          ),
          onPressed: () {
            if (widget.title == ".") {
              if (hasDot == false) {
                setState(() {
                  hasDot = true;
                });
                widget.controller.text += widget.title;
              } else {}
            } else {
              widget.controller.text += widget.title;
            }
          },
          child: Center(
            child: Text(
              widget.title,
              style: const TextStyle(
                color: Colors.black,
                fontSize: 18,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
