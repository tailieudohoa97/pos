import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_pos_122219/ManageCash.dart';
import 'package:my_pos_122219/OrderHistory.dart';
import 'package:my_pos_122219/RegisterMain.dart';
import 'package:my_pos_122219/TodayProfit.dart';
import './TextStyle.dart';
import 'dart:html' as html;

import 'main.dart';

void requestFullScreen() {
  html.document.documentElement?.requestFullscreen();
}

void exitFullScreen() {
  html.document.exitFullscreen();
}

//Screen Start
class Header extends StatefulWidget {
  const Header({super.key});

  @override
  State<Header> createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  bool fullScreen = false;
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    // double screenHeight = MediaQuery.of(context).size.height;
    // ignore: unused_local_variable
    // Future<void> screenDisplay;

    // .........
    // KHUNG LỚN NHẤT
    // .........
    return Material(
      child: Container(
        width: screenWidth,
        height: 50,
        decoration: const BoxDecoration(
          color: Color(0xFF435756),
        ),
        alignment: Alignment.center,
        // .........
        // KHUNG NHỎ
        // .........
        child: SingleChildScrollView(
          // scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //Row left
              Row(
                children: [
                  //Menu Icon
                  // const MenuIcon(),
                  const Menumain(),

                  //Store Icon
                  const SizedBox(
                    width: 50,
                    height: 50,
                    child: Icon(Icons.store, color: Colors.white),
                  ),

                  //Store Name
                  Container(
                    margin: const EdgeInsets.only(right: 20.0),
                    child: const Text.rich(
                      TextSpan(
                        text: 'Store: ',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        children: [
                          TextSpan(
                            text: 'Food Store',
                            style: TextStyle(
                              fontWeight: FontWeight.w100,
                            ),
                          ),
                          // TextSpan(text: 'Food Store'),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 20.0),
                    child: const Text.rich(
                      TextSpan(
                        text: 'Register: ',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        children: [
                          TextSpan(
                            text: 'Register 2',
                            style: TextStyle(fontWeight: FontWeight.w100),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),

              //Row Right
              Row(
                children: [
                  //Bell icon
                  SizedBox(
                    width: 50,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () {
                        // ignore: avoid_print
                        print('Sound');
                      },
                      style: TextButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0),
                        ),
                        foregroundColor: Colors.white,
                        backgroundColor: const Color.fromARGB(0, 255, 255, 255),
                      ),
                      child: const IconButton(
                        onPressed: null,
                        icon: Icon(Icons.notifications, color: Colors.white),
                        tooltip: "Sound",
                        mouseCursor: SystemMouseCursors.click,
                      ),
                    ),
                  ),

                  //Zoom/ FullScreen icon
                  SizedBox(
                    width: 50,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0),
                        ),
                        foregroundColor: Colors.white,
                        backgroundColor: const Color(0x00000000),
                      ),
                      child: IconButton(
                        onPressed: (() {
                          fullScreen = !fullScreen;
                          if (fullScreen == true) {
                            exitFullScreen();
                          } else {
                            requestFullScreen();
                          }
                        }),
                        icon: const Icon(Icons.zoom_out_map_sharp,
                            color: Colors.white),
                        tooltip: "Full Screen",
                        mouseCursor: SystemMouseCursors.click,
                      ),
                    ),
                  ),
                  const MyAccount(),
                  //Login button
                  SizedBox(
                    width: 70,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () {
                        // ignore: avoid_print
                        print('Logout');
                      },
                      style: ButtonStyle(
                        shape: MaterialStatePropertyAll(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            // side: const BorderSide(color: Colors.red),
                          ),
                        ),
                        foregroundColor: const MaterialStatePropertyAll(
                          Colors.white,
                        ),
                        backgroundColor: const MaterialStatePropertyAll(
                          Color.fromRGBO(198, 83, 56, 1),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(
                            Icons.login_rounded,
                            color: Colors.white,
                          ),
                          Text(
                            'Logout',
                            style: TextStyle(fontSize: 11, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MyAccount extends StatelessWidget {
  const MyAccount({super.key});

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      color: const Color(0xFF09adaa),
      position: PopupMenuPosition.under,
      offset: const Offset(0, 7),
      child: IntrinsicWidth(
        child: Container(
          // color: Colors.orange,
          // width: 200,
          height: 50,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Image.network(
                  'https://secure.gravatar.com/avatar/f3eccd11d972f48efdc249be5e6a94d1?s=140&d=mm&r=g',
                  width: 32,
                  height: 32,
                ),
                const SizedBox(
                  width: 10,
                ),
                const Text(
                  'Nguyễn Trần Minh Kha',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          padding: const EdgeInsets.all(0),
          textStyle: const TextStyle(
            fontSize: 11,
            color: Colors.white,
          ),
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: 170,
            ),
            child: Column(
              children: [
                _buildRow("Login time", "None"),
                _buildRow("Orders", "None"),
                _buildRow("Products", "None"),
                _buildRow("Total Sales", "None"),
                _buildDownloadButton(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildRow(String label, String value) {
    return SizedBox(
      height: 40,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              label,
            ),
            Text(
              value,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDownloadButton() {
    return Container(
      color: const Color(0xFF089d9a),
      height: 40,
      padding: const EdgeInsets.all(0),
      alignment: Alignment.center,
      child: Column(
        children: [
          TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: const Color(0xFF089d9a),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 20,
              ),
              textStyle: const TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.bold,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Download CSV".toUpperCase(),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    // return InkWell(
    //   onTap: () {},
    //   splashColor: const Color(0xFF089d9a),
    //   hoverColor: const Color(0xFF089d9a),
    //   child: Container(
    //     height: 40,
    //     color: const Color(0xFF089d9a),
    //     padding: const EdgeInsets.all(15),
    //     child: Row(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       crossAxisAlignment: CrossAxisAlignment.center,
    //       children: [
    //         const Icon(
    //           Icons.download,
    //           size: 16,
    //           color: Colors.white,
    //         ),
    //         const SizedBox(width: 10),
    //         Text(
    //           'Download CSV'.toUpperCase(),
    //           style: const TextStyle(
    //             fontWeight: FontWeight.bold,
    //           ),
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}

class Menumain extends StatelessWidget {
  const Menumain({super.key});

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      color: const Color(0xFF09adaa),
      position: PopupMenuPosition.under,
      offset: const Offset(0, 7),
      child: const MenuIcon(),
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          padding: const EdgeInsets.all(0),
          textStyle: const TextStyle(
            fontSize: 11,
            color: Colors.white,
          ),
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: 170,
            ),
            child: Column(
              children: [
                _iconTextMenu(Icons.app_registration_sharp, 'Register Screen',
                    const RegisterScreen(), context),
                _iconTextMenu(Icons.history, 'Order History',
                    const OrderHistory(), context),
                _iconTextMenu(Icons.donut_small_sharp, "Today's profit",
                    const TodayProfit(), context),
                _iconTextMenu(Icons.currency_exchange_sharp, 'Manage Cash',
                    const ManageCash(), context),
                _buildCloseRegisterButton(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _iconTextMenu(
      IconData icon, String name, Widget x, BuildContext context) {
    return InkWell(
      onTap: () {
        if (x.toString() == "OrderHistory\$") {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  const OrderHistory(),
              transitionsBuilder:
                  (context, animation, secondaryAnimation, child) {
                return FadeTransition(
                  opacity: animation,
                  child: child,
                );
              },
            ),
          );
        } else if (x.toString() != "RegisterScreen" &&
            x.toString() != "OrderHistory\$") {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  content: x,
                  contentPadding: EdgeInsets.zero,
                );
              });
        }
      },
      splashColor: const Color(0xFF089d9a),
      hoverColor: const Color(0xFF089d9a),
      child: Container(
        height: 45,
        padding: const EdgeInsets.all(15),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 18,
              color: Colors.white,
            ),
            const SizedBox(width: 10),
            Text(
              name.toUpperCase(),
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCloseRegisterButton() {
    return InkWell(
      onTap: () {},
      splashColor: const Color(0xFF089d9a),
      hoverColor: const Color(0xFF089d9a),
      child: Container(
        height: 45,
        color: const Color(0xFFc65338),
        padding: const EdgeInsets.all(15),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(
              Icons.logout,
              size: 18,
              color: Colors.white,
            ),
            const SizedBox(width: 10),
            Text(
              'Logout'.toUpperCase(),
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MenuIcon extends StatefulWidget {
  const MenuIcon({super.key});

  @override
  State<MenuIcon> createState() => _MenuIconState();
}

class _MenuIconState extends State<MenuIcon> {
  bool hover = true;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 50,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        onHover: (event) {
          setState(() {
            hover = false;
          });
        },
        onExit: ((event) {
          setState(() {
            hover = true;
          });
        }),
        child: ElevatedButton(
          onPressed: null,
          style: TextButton.styleFrom(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            padding: const EdgeInsets.all(0),
            foregroundColor: Colors.white,
            backgroundColor: const Color(0xFF09adaa),
            disabledForegroundColor: Colors.white,
          ),
          child: Icon((hover == false) ? Icons.close : Icons.menu),
        ),
      ),
    );
  }
}
