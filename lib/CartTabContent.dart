import 'package:flutter/material.dart'; //File nào cũng phải có
import 'package:intl/intl.dart'; //Format số thập phân - NumberFormat.currency
import './/RemoveButton.dart';
import './EditButton.dart';
import 'package:quantity_input/quantity_input.dart'; //Nút tăng giảm số lượng
import 'package:flutter/services.dart'; //Sử dụng cho nhập liệu TextField - FilteringTextInputFormatter

class CartTabContent extends StatelessWidget {
  const CartTabContent({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          LayoutProductInCart(
            image: 'sp1.png',
            name: 'Apple Pie',
            price: 5.50,
            total: 5.50,
          ),
          LayoutProductInCart(
            image: 'sp2.png',
            name: 'Blueberry Cupcake',
            price: 6.50,
            total: 6.50,
          ),
          LayoutProductInCart(
            image: 'sp3.png',
            name: 'Chocolate Cupcake',
            price: 7.00,
            total: 7.00,
          ),

          //CART'S CONTENT
          //This widget appears when Cart is empty
          //Widget này khiến Nội dung ko canh giữa trên dưới được
          // SingleChildScrollView(
          //   child: Column(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       Icon(
          //         Icons.add_shopping_cart_rounded,
          //         size: 250,
          //         color: Colors.black.withOpacity(0.4),
          //       ),
          //       Text(
          //         "Add the first product to the cart",
          //         style: TextStyle(
          //             fontWeight: FontWeight.bold,
          //             color: Colors.black.withOpacity(0.4),
          //             fontSize: 16),
          //       )
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}

//Layout Product in Cart
class LayoutProductInCart extends StatelessWidget {
  final String image;
  final String name;
  final double price;
  final double total;
  const LayoutProductInCart({
    Key? key,
    required this.image,
    required this.name,
    required this.price,
    required this.total,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final priceFormatter = NumberFormat.currency(
      symbol: '\$',
      decimalDigits: 2,
    );

    return ExpansionTile(
      tilePadding: const EdgeInsets.only(right: 20, top: 0),
      childrenPadding: const EdgeInsets.symmetric(horizontal: 20),
      // trailing: const Icon(null),
      backgroundColor: const Color(0xFFf5f5f5),

      title: Container(
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: Color(0xFFdbdbdb),
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Row Left
            Row(
              children: [
                //Remove button
                const RemoveButton(),

                //Thumbnail
                Image.asset(
                  image,
                  width: 50,
                  fit: BoxFit.cover,
                ),
                const SizedBox(width: 5),

                //Note icon
                const Icon(
                  Icons.message,
                  size: 20,
                ),
                const SizedBox(width: 5),

                //Pro Name
                Text(
                  name,
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),

            // Row right
            Row(
              children: [
                const EditButton(),
                const QuantityInputPro(),
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minWidth: 70,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        priceFormatter.format(price),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFF888888),
                        ),
                      ),
                    ],
                  ),
                ),
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minWidth: 100,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        priceFormatter.format(total),
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: const [
            Text(
              'Change price per unit',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: Color(0xFF707070),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              height: 35,
              width: 180,
              color: Colors.white,
              child: TextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
                ],
                decoration: const InputDecoration(
                  hintText: 'Change price this product...',
                  hintStyle: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w100,
                  ),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFdbdbdb)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF09ADAA)),
                  ),
                  // border: OutlineInputBorder(),
                ),
                cursorColor: Colors.black,
                cursorHeight: 20,
                style: const TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 12,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: const [
            Icon(
              Icons.message,
              size: 15,
            ),
            SizedBox(width: 5),
            Text(
              'Edit note',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: Color(0xFF707070),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              width: 450,
              color: Colors.white,
              child: const TextField(
                maxLines: 3,
                decoration: InputDecoration(
                  hintText: 'Note here...',
                  hintStyle: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w100,
                  ),
                  contentPadding: EdgeInsets.all(10),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFdbdbdb)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF09ADAA)),
                  ),
                  // border: OutlineInputBorder(),
                ),
                cursorColor: Colors.black,
                cursorHeight: 20,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 12,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              onPressed: () {},
              style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: const Color(0xFF09adaa),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                textStyle: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
              child: Text(
                'update'.toUpperCase(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 30),
      ],
    );
  }
}

//Quantity button
class QuantityInputPro extends StatefulWidget {
  const QuantityInputPro({super.key});

  @override
  State<QuantityInputPro> createState() => _QuantityInputProState();
}

class _QuantityInputProState extends State<QuantityInputPro> {
  int simpleIntInput = 1;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        QuantityInput(
          // label: 'Simple int input',
          value: simpleIntInput,
          onChanged: (value) => setState(
            () => simpleIntInput = int.parse(
              value.replaceAll(',', ''),
            ),
          ),
          buttonColor: const Color(0xFF09adaa),
          iconColor: Colors.white,
          inputWidth: 50,
        ),
        // Text(
        //   'Value: $simpleIntInput',
        //   style:
        //       const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        // ),
      ],
    );
  }
}
