import 'package:flutter/material.dart';
import './TextStyle.dart';

class AddNewProduct extends StatefulWidget {
  const AddNewProduct({super.key});

  @override
  State<AddNewProduct> createState() => _AddNewProductState();
}

class _AddNewProductState extends State<AddNewProduct> {
  String errorMessage_Name = "";
  String errorMessage_Price = "";
  RegExp doubleRegex = RegExp(r'^-?[0-9]+([.][0-9]*)?$');

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
        width: 740,
        padding: const EdgeInsets.all(20),
        child: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Title
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.black.withOpacity(0.2),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Add a new product",
                      style: TextStyleGlobal.titlePopupBlackTextStyle,
                    ),

                    //Close button
                    CloseButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: NameTextField(
                      handleOnChange: (value) {
                        if (value == "") {
                          setState(() {
                            errorMessage_Name = "This field is required";
                          });
                        } else {
                          setState(() {
                            errorMessage_Name = "";
                          });
                        }
                      },
                      errorMessage: errorMessage_Name,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: PriceTextField(
                      handleOnChange: (value) {
                        if (value == "") {
                          setState(() {
                            errorMessage_Price = "This field is required";
                          });
                        } else if (!doubleRegex.hasMatch(value)) {
                          setState(() {
                            value = "";
                            errorMessage_Price =
                                "Please enter a number. If decimal, use the decimal separator (.). Never use thousand separators.";
                          });
                        } else {
                          setState(() {
                            errorMessage_Price = "";
                          });
                        }
                      },
                      errorMessage: errorMessage_Price,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  const Expanded(
                    child: TaxStatus(),
                  ),
                ],
              ),
              const TaxClass(),
              const SizedBox(
                height: 20,
              ),

              //Create Product Button
              SizedBox(
                height: 40,
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color(0xFF09ADAA),
                    shape: RoundedRectangleBorder(
                      side: const BorderSide(
                        color: Color.fromRGBO(9, 173, 170, 1),
                        width: 0.2,
                      ),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    foregroundColor: Colors.white,
                  ),
                  child: Text(
                    'Create Product'.toUpperCase(),
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NameTextField extends StatelessWidget {
  final Function handleOnChange;
  final String errorMessage;
  const NameTextField({
    Key? key,
    required this.handleOnChange,
    required this.errorMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Product Name: *",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (value) => handleOnChange(value),
            decoration: InputDecoration(
              // errorText: 'This field is required',
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Color(0xFFdbdbdb),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color:
                      errorMessage == "" ? const Color(0xFF09ADAA) : Colors.red,
                ),
              ),
            ),
            cursorColor: Colors.black,
          ),
        ),
        if (errorMessage != "")
          Text(
            errorMessage,
            style: const TextStyle(color: Colors.red, fontSize: 12),
          )
      ],
    );
  }
}

class PriceTextField extends StatelessWidget {
  final Function handleOnChange;
  final String errorMessage;
  const PriceTextField({
    Key? key,
    required this.handleOnChange,
    required this.errorMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Regular Price: *",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (value) => handleOnChange(value),
            decoration: InputDecoration(
                // errorText: 'This field is required',
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        width: 1,
                        color: errorMessage == ""
                            ? const Color(0xFF09ADAA)
                            : Colors.red))),
            cursorColor: Colors.black,
          ),
        ),
        if (errorMessage != "")
          Text(
            errorMessage,
            style: const TextStyle(color: Colors.red, fontSize: 12),
          )
      ],
    );
  }
}

class TaxStatus extends StatefulWidget {
  const TaxStatus({super.key});

  @override
  State<TaxStatus> createState() => _TaxStatus();
}

class _TaxStatus extends State<TaxStatus> {
  String shippingMethod = 'Taxable';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = ['Taxable', 'Shipping only', 'None'];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Tax status:",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: const Text('Choose...'),
                              value: shippingMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                setState(() {
                                  shippingMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class TaxClass extends StatefulWidget {
  const TaxClass({super.key});

  @override
  State<TaxClass> createState() => _TaxClass();
}

class _TaxClass extends State<TaxClass> {
  String? shippingMethod;

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = ['Hà Nội', 'Hồ Chí Minh'];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Tax class:",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: const Text('Choose...'),
                              value: shippingMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                setState(() {
                                  shippingMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
