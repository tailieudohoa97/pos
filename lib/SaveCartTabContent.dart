import 'package:flutter/material.dart'; //File nào cũng phải có
import 'package:intl/intl.dart'; //Format số thập phân - NumberFormat.currency
import 'package:flutter/services.dart'; //Sử dụng cho nhập liệu TextField - FilteringTextInputFormatter
import './RemoveButton.dart';
import './EditButton.dart';

class SaveCartTabContent extends StatelessWidget {
  const SaveCartTabContent({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          LayoutProductInCart(
            idpro: 111,
            name: 'By Guest Customer',
            quantity: 5,
            price: 5.50,
          ),
          LayoutProductInCart(
            idpro: 222,
            name: 'By Guest Customer',
            quantity: 6,
            price: 6.50,
          ),
          LayoutProductInCart(
            idpro: 333,
            name: 'By Guest Customer',
            quantity: 7,
            price: 7.00,
          ),
        ],
      ),
    );
  }
}

//Layout Product in Cart
class LayoutProductInCart extends StatelessWidget {
  final int idpro;
  final String name;
  final int quantity;
  final double price;

  const LayoutProductInCart({
    Key? key,
    required this.idpro,
    required this.name,
    required this.quantity,
    required this.price,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final priceFormatter = NumberFormat.currency(
      symbol: '\$',
      decimalDigits: 2,
    );

    return ExpansionTile(
      tilePadding: const EdgeInsets.only(right: 20, top: 0),
      childrenPadding: const EdgeInsets.symmetric(horizontal: 20),
      // trailing: const Icon(null),
      backgroundColor: const Color(0xFFf5f5f5),

      title: Container(
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: Color(0xFFdbdbdb),
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Row Left
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //Remove button
                const RemoveButton(),

                //Icon
                const Icon(
                  Icons.shopping_cart_checkout_rounded,
                  color: Color(0xFFe09914),
                ),
                const SizedBox(width: 5),

                //Note icon
                const Icon(
                  Icons.message,
                  size: 20,
                ),
                const SizedBox(width: 5),

                //Pro Name
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '#$idpro',
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      name,
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),

            // Row right
            Row(
              children: [
                //Edit Button
                const EditButton(),

                //Quantity
                Text(
                  ('$quantity products'),
                  style: const TextStyle(
                    fontSize: 12,
                    color: Colors.black,
                  ),
                ),

                //Cart's Status
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      width: 1.0,
                    ),
                    color: Colors.white,
                  ),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: const IntrinsicWidth(
                    child: Text(
                      'Pending payment',
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),

                //Price
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minWidth: 70,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          priceFormatter.format(price),
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF888888),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                //Reload button
                TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFF09adaa),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.refresh,
                        size: 13,
                      ),
                      const SizedBox(width: 5),
                      Text(
                        'load'.toUpperCase(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: const [
            Icon(
              Icons.message,
              size: 15,
            ),
            SizedBox(width: 5),
            Text(
              'Edit note',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: Color(0xFF707070),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              width: 450,
              color: Colors.white,
              child: const TextField(
                maxLines: 3,
                decoration: InputDecoration(
                  hintText: 'Note here...',
                  hintStyle: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w100,
                  ),
                  contentPadding: EdgeInsets.all(10),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFdbdbdb)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF09ADAA)),
                  ),
                  // border: OutlineInputBorder(),
                ),
                cursorColor: Colors.black,
                cursorHeight: 20,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 12,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              onPressed: () {},
              style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: const Color(0xFF09adaa),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                textStyle: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
              child: Text(
                'update'.toUpperCase(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 30),
      ],
    );
  }
}
