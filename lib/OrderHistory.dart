import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:my_pos_122219/PrintReceipt.dart';
import 'package:my_pos_122219/ScreenStart.dart';
import 'package:my_pos_122219/header.dart';

import 'RegisterMain.dart';

class OrderHistory extends StatelessWidget {
  const OrderHistory({super.key});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width / 2;
    return Material(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            const Header(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(child: LeftPanel()),
                SizedBox(width: width, child: PrintReceipt())
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class LeftPanel extends StatelessWidget {
  const LeftPanel({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    PageRouteBuilder(
                      pageBuilder: (context, animation, secondaryAnimation) =>
                          const RegisterScreen(),
                      transitionsBuilder:
                          (context, animation, secondaryAnimation, child) {
                        return FadeTransition(
                          opacity: animation,
                          child: child,
                        );
                      },
                    ),
                  );
                },
                style: ElevatedButton.styleFrom(
                  alignment: const Alignment(-1, 1),
                  backgroundColor: const Color.fromRGBO(234, 234, 234, 1),
                  shape: RoundedRectangleBorder(
                    side: const BorderSide(
                        color: Color.fromRGBO(234, 234, 234, 1), width: 0.2),
                    borderRadius: BorderRadius.circular(0),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    '< BACK TO REGISTER SCREEN'.toUpperCase(),
                    style: const TextStyle(
                        fontSize: 20, color: Color.fromRGBO(9, 173, 170, 1)),
                  ),
                ),
              ),
            )
          ],
        ),
        const SelectRegister(),
        const OrderDetail(
          items: 2,
          selectedIndex: 0,
        ),
        const SizedBox(
          height: 3,
        ),
        const OrderDetail(
          items: 2,
          selectedIndex: -1,
        ),
        const SizedBox(
          height: 3,
        ),
        const OrderDetail(
          items: 2,
          selectedIndex: -1,
        ),
        const SizedBox(
          height: 3,
        )
      ],
    );
  }
}

class SelectRegister extends StatefulWidget {
  const SelectRegister({super.key});

  @override
  State<SelectRegister> createState() => SelectRegisterState();
}

// ignore: camel_case_types
class SelectRegisterState extends State<SelectRegister> {
  String chooseRegister = 'Choose a Register';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseRegister;
  List listItem = [
    'Choose a Register',
    'Register 1',
    'Register 2',
    'Register 3'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 45,
      // padding: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          // color: const Color.fromRGBO(9, 173, 170, 1),
          color: const Color.fromRGBO(234, 234, 234, 1),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Stack(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          // alignedDropdown: true là chiều rộng của menu DropdownButton sẽ khớp với chiều rộng của nút.
                          // alignedDropdown: true,
                          child: DropdownButton(
                            //Ban đầu rỗng, sẽ hiển thị cái hint

                            isExpanded: true,
                            underline: const SizedBox(),
                            style: const TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                            ),
                            icon: const Icon(Icons.keyboard_arrow_down),
                            hint: const Text('Choose a Store'),
                            value: chooseRegister,
                            // Mỗi lần click chọn 1 item
                            // thì hàm Onchange sẽ sét lại value mới = value mới vừa chọn
                            // dẫn tới "value: chooseRegister," ở phía trên thay đổi
                            // Và cái hint sẽ bị thay = cái string item đã chọn
                            onChanged: (newValue) {
                              setState(() {
                                chooseRegister = newValue
                                    .toString(); //Phải cùng kiểu String >>> .toString()
                              });
                            },

                            //Tạo list item, layout cho 1 item
                            items: listItem.map((newValue) {
                              return DropdownMenuItem(
                                value: newValue,
                                child: Text(newValue),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OrderDetail extends StatefulWidget {
  const OrderDetail(
      {super.key, required this.items, required this.selectedIndex});
  final int items;
  final int selectedIndex;
  @override
  State<OrderDetail> createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  bool _isExpanded = false;

  final Icon x = const Icon(
    Icons.keyboard_arrow_down,
    color: Colors.white,
  );
  final Icon y = const Icon(
    Icons.keyboard_arrow_up,
    color: Colors.white,
  );

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      tilePadding: const EdgeInsets.all(20),

      // trailing: const Icon(null),
      backgroundColor: const Color.fromRGBO(9, 173, 170, 1),
      collapsedBackgroundColor: const Color.fromRGBO(67, 87, 86, 1),
      iconColor: Colors.white,
      trailing: _isExpanded ? y : x,
      onExpansionChanged: (value) {
        setState(() {
          _isExpanded = value;
        });
      },
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "February 19, 2023",
            style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.w600, color: Colors.white),
          ),
          Row(
            children: const [
              Text(
                "2 orders",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              Icon(
                Icons.insert_chart,
                color: Colors.white,
              ),
            ],
          )
        ],
      ),
      children: [
        Detail(
          items: widget.items,
          selectedIndex: widget.selectedIndex,
        ),
      ],
    );
    ;
  }
}

class OrderRow extends StatelessWidget {
  final String order_id;
  final String customer;
  final String date_time;
  final String status;
  final Color color;
  const OrderRow(
      {super.key,
      required this.order_id,
      required this.customer,
      required this.date_time,
      required this.status,
      required this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              const Icon(
                Icons.keyboard_arrow_right,
                color: Colors.black,
              ),
              Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "Order #",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: color),
                      ),
                      Text(
                        order_id,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: color),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(
                        width: 25,
                      ),
                      Text(
                        date_time + "",
                        style: const TextStyle(
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        status,
                        style: const TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        "Customer:",
                        style: TextStyle(fontSize: 15),
                      ),
                      Text(
                        customer,
                        style: const TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          Column(
            children: const [
              Text(
                "\$314",
                style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                    color: Colors.black),
              ),
              Text(
                "1 item",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: Colors.black),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Detail extends StatefulWidget {
  final int items;
  final int selectedIndex;
  const Detail({super.key, required this.items, required this.selectedIndex});

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  int _selectedIndex = -1;

  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.items * 104,
      child: ListView.builder(
        itemCount: widget.items,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                if (widget.selectedIndex == 0) {
                  _selectedIndex = 0;
                }
                _selectedIndex = index;
              });
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 0),
              child: Container(
                decoration: BoxDecoration(
                  color: _selectedIndex == index
                      ? const Color.fromRGBO(234, 234, 234, 1)
                      : Colors.white,
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    color: Colors.grey,
                    width: 2.0,
                  ),
                ),
                child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 0),
                    child: OrderRow(
                      customer: "Guest",
                      date_time: "3:42 AM",
                      order_id: "23456",
                      status: "Completed",
                      color: _selectedIndex == index
                          ? const Color.fromRGBO(9, 173, 170, 1)
                          : Colors.black,
                    )),
              ),
            ),
          );
        },
      ),
    );
  }
}
