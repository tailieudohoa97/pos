import 'package:flutter/material.dart'; //File nào cũng phải có

//CUSTOMER'S CONTENT
class CustomerTabContent extends StatelessWidget {
  const CustomerTabContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          //Proceed as guest
          TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
              foregroundColor: Colors.black,
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
                side: const BorderSide(
                  width: 1,
                  color: Color(0xFFf5f5f5),
                ),
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 20,
              ),
              textStyle: const TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
            child: Row(
              children: [
                const Icon(
                  Icons.person,
                  size: 16,
                ),
                const SizedBox(width: 5),
                Text('Proceed as guest'.toUpperCase())
              ],
            ),
          ),
          const SizedBox(height: 10),

          //Load a customer profile
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                width: 1,
                color: const Color(0xFFf5f5f5),
              ),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    const Icon(
                      Icons.face,
                      size: 16,
                    ),
                    const SizedBox(width: 5),
                    Text(
                      'Load a customer profile'.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 10),
                Container(
                  // height: 35,
                  color: Colors.white,
                  child: const TextField(
                    decoration: InputDecoration(
                      helperText: 'Please enter 3 or more characters',
                      hintText: 'Search for a customer...',
                      hintStyle: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w100,
                      ),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(9, 173, 170, 1),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromARGB(255, 173, 148, 9),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      border: OutlineInputBorder(),
                    ),
                    // clipBehavior: Clip.hardEdge,
                    cursorColor: Colors.black,
                    cursorHeight: 20,
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 12,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
