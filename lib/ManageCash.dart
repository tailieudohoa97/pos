import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'Numpad.dart';
import 'TextStyle.dart';

class ManageCash extends StatefulWidget {
  const ManageCash({super.key});

  @override
  State<ManageCash> createState() => _ManageCashState();
}

class _ManageCashState extends State<ManageCash> {
  final TextEditingController _myController = TextEditingController();
  String _type = "Remove";
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.black)),
        width: 540,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Add cash in hand",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 18)),
                    IconButton(
                        onPressed: (() {
                          Navigator.pop(context);
                        }),
                        icon: const Icon(Icons.close))
                  ],
                ),
              ),
              Divider(
                height: 10,
                thickness: 2,
                color: Colors.black.withOpacity(0.2),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CashMethod(
                      handleOnChange: (value) {
                        setState(() {
                          _type = value;
                        });
                      },
                      type: _type),
                  const SizedBox(
                    width: 35,
                  ),
                  CiHTextField(
                    controller: _myController,
                  )
                ],
              ),
              const ReasonTextField(),
              NumPad(
                submitText: "SAVE CASH",
                buttonSize: 75,
                buttonColor: Colors.white,
                iconColor: const Color.fromRGBO(9, 173, 170, 1),
                controller:
                    _myController, // controller trỏ đến text field nào thì khi thao tác với các phím số sẽ ghi thêm giá trị vào text field đó
                // hàm xóa 1 kí tự
                delete: () {
                  _myController.text = _myController.text
                      .substring(0, _myController.text.length - 1);
                },
                //hàm xóa toàn bộ kí tự
                clear: () {
                  _myController.text = "";
                },
                // do something with the input numbers
                onSubmit: () {
                  debugPrint('Your code: ${_myController.text}');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CiHTextField extends StatelessWidget {
  final TextEditingController controller;
  const CiHTextField({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Text("Cash in hand",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 10)),
          ),
          const SizedBox(
            height: 10,
          ),
          TextField(
            controller: controller,
            decoration: const InputDecoration(
                prefixIcon: Icon(
                  Icons.attach_money,
                  color: Colors.black,
                ),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: Color(0xFF09ADAA)))),
            cursorColor: Colors.black,
          ),
        ],
      ),
    );
  }
}

class ReasonTextField extends StatelessWidget {
  const ReasonTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text("Reason",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 10)),
        ),
        TextField(
          decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFF09ADAA)))),
          cursorColor: Colors.black,
        ),
      ],
    );
  }
}

class CashMethod extends StatelessWidget {
  final Function handleOnChange;
  final String type;
  const CashMethod(
      {super.key, required this.handleOnChange, required this.type});

  @override
  Widget build(BuildContext context) {
    List listItem = [
      'Remove',
      'Add',
    ];
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: Text(
              "Amount type",
              textAlign: TextAlign.left,
              style: TextStyleGlobal.regBlackTextStyle,
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
            width: 150,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                // color: const Color.fromRGBO(9, 173, 170, 1),
                color: const Color.fromRGBO(234, 234, 234, 1),
              ),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Icon(Icons.qr_code_scanner),
                                  SizedBox(width: 5),
                                  Text('Remove'),
                                ],
                              ),
                              value: type,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                handleOnChange(newValue);
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
