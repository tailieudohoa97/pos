import 'package:flutter/material.dart';
import './TextStyle.dart';

class AddCoupon extends StatefulWidget {
  final double orderAmount;
  const AddCoupon({super.key, required this.orderAmount});

  @override
  State<AddCoupon> createState() => _AddCouponState();
}

class _AddCouponState extends State<AddCoupon> {
  String isAvailable = "start";
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
        width: 540,
        child: IntrinsicWidth(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Close button
              Container(
                alignment: Alignment.topRight,
                child: CloseButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),

              //Title
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                padding: const EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.black.withOpacity(0.2),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Apply coupon to order total",
                      style: TextStyleGlobal.titlePopupBlackTextStyle,
                    ),
                    Text(
                      "\$${widget.orderAmount}",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(9, 173, 170, 1),
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),

              CodeTextField(handleOnChange: (value) {
                setState(() {
                  if (value == 123456) {
                    isAvailable = "true";
                  } else if (value == "") {
                    isAvailable = "start";
                  } else {
                    isAvailable = "false";
                  }
                });
              }),
              isAvailable != "false" ? const DetailCoupon() : _CouponNotFound(),
              const AmountToPay(),

              const SaveNoteButton()
            ],
          ),
        ),
      ),
    );
  }

  Widget _CouponNotFound() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(color: Color.fromRGBO(240, 240, 240, 1)),
      child: const Text(
        "Coupon not found",
        style: TextStyle(color: Colors.red),
      ),
    );
  }
}

class CodeTextField extends StatelessWidget {
  final Function handleOnChange;
  const CodeTextField({super.key, required this.handleOnChange});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Enter coupon code",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            onChanged: (value) => handleOnChange(value),
            decoration: const InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFF09ADAA))),
            ),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class DetailCoupon extends StatelessWidget {
  const DetailCoupon({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(top: 20),
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: const Color(0xFFF0F0F0),
              borderRadius: const BorderRadius.all(Radius.circular(5)),
              border: Border.all(
                color: const Color(0xFFD6D6D6),
                width: 1.0,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "Discount type: ",
                  style: TextStyleGlobal.regBlackTextStyle,
                ),
                Text(
                  "Coupon amount: ",
                  style: TextStyleGlobal.regBlackTextStyle,
                ),
                Text(
                  "Expiry date: ",
                  style: TextStyleGlobal.regBlackTextStyle,
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class AmountToPay extends StatelessWidget {
  const AmountToPay({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Amount to pay",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                height: 40,
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Color(0xFFdbdbdb),
                      style: BorderStyle.solid,
                    ),
                  ),
                ),
                alignment: Alignment.centerRight,
                child: const Text(
                  "\$0.00",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF09ADAA),
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class SaveNoteButton extends StatelessWidget {
  const SaveNoteButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 0),
      padding: const EdgeInsets.only(top: 20),
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(
            child: ElevatedButton(
              onPressed: () {},
              style: ButtonStyle(
                shape: MaterialStatePropertyAll(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    // side: const BorderSide(color: Colors.red),
                  ),
                ),
                foregroundColor: const MaterialStatePropertyAll(
                  Colors.white,
                ),
                padding: const MaterialStatePropertyAll(
                  EdgeInsets.all(20),
                ),
                backgroundColor: const MaterialStatePropertyAll(
                    Color.fromRGBO(9, 173, 170, 1)),
              ),
              child: Text(
                'Apply coupon'.toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
