import 'package:flutter/material.dart';

class TextStyleGlobal {
  //Title Page: <h1>
  static const TextStyle xlgBlackTextStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );

  //Heading
  static const TextStyle lgBlackTextStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w800,
    color: Colors.black,
  );

  //Title, label popup
  static const TextStyle regBlackTextStyle = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w600,
    color: Colors.black,
  );

  //Title
  static const TextStyle regWhiteTextStyle = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w600,
    color: Colors.white,
  );

  //Content
  static const TextStyle smBlackTextStyle = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w300,
    color: Colors.black,
  );

  //
  static const TextStyle xsmBlackTextStyle = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w300,
    color: Colors.grey,
  );

  //////////////////////////
  //FORM
  //////////////////////////
  //Form: Label, Content
  static const TextStyle formTextStyle = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w300,
    color: Color.fromRGBO(9, 173, 170, 1),
  );

  //////////////////////////
  //PRODUCT
  //////////////////////////
  //Title + Price of Product
  static const TextStyle titlePriceProWhite = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    color: Colors.white,
  );

  //////////////////////////
  //POPUP
  //////////////////////////
  //Title Popup
  static const TextStyle titlePopupBlackTextStyle = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w600,
    color: Colors.black,
  );
}
