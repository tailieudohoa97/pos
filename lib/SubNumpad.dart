import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class SubNumPad extends StatefulWidget {
  SubNumPad({
    Key? key,
  }) : super(key: key);

  @override
  State<SubNumPad> createState() => _SubNumPadState();
}

class _SubNumPadState extends State<SubNumPad> {
  TextEditingController controller = TextEditingController();
  String input = "0";
  String answer = "0";
  void initState() {
    super.initState();
    controller.addListener(_onTextChanged);
  }

  void _onTextChanged() {
    // Handle the text change event here
    setState(() {
      if (controller.text.isEmpty) {
        input = "0";
      } else {
        input = controller.text;
      }
      //print(ShippingFee);
    });
  }

// function to calculate the input operation
  void equalPressed() {
    String finaluserinput = input;
    finaluserinput = input.replaceAll('x', '*');

    Parser p = Parser();
    Expression exp = p.parse(finaluserinput);
    ContextModel cm = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL, cm);
    answer = eval.toString();
    controller.text = "";
    input = "0";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: Color.fromRGBO(244, 244, 244, 1)),
      width: 320,
      height: 350,
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                    child: Container(
                  height: 56,
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: Text(
                          answer.toString(),
                          style: const TextStyle(color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: Text(
                          input.toString(),
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                )),
                Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: SizedBox(
                    height: 56,
                    width: 56,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Color.fromRGBO(9, 173, 170, 1),
                                width: 0.2),
                            borderRadius: BorderRadius.circular(0),
                          ),
                        ),
                        onPressed: () {
                          controller.text = "";
                          input = "0";
                          answer = "0";
                        }, // Gọi hàm delete khi bấm nút này
                        child: const Center(child: Icon(Icons.undo))),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // implement the number keys (from 0 to 9) with the NumberButton widget
            // the NumberButton widget is defined in the bottom of this file
            children: [
              Padding(
                padding: const EdgeInsets.all(4),
                child: NumberButton(
                  number: 1,
                  controller: controller,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 2, 2, 2),
                child: NumberButton(
                  number: 2,
                  controller: controller,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(2, 2, 2, 2),
                child: NumberButton(
                  number: 3,
                  controller: controller,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(2),
                child: SizedBox(
                  height: 56,
                  width: 56,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                            color: Color.fromRGBO(9, 173, 170, 1), width: 0.2),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        controller.text = "";
                        input = "0";
                        answer = "0";
                      });
                    },
                    child: const Center(
                      child: Text(
                        "C",
                        style: TextStyle(
                            color: Color.fromRGBO(9, 173, 170, 1),
                            fontSize: 12),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 2, 6, 2),
                  child: SizedBox(
                    height: 56,
                    width: 56,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Color.fromRGBO(9, 173, 170, 1),
                                width: 0.2),
                            borderRadius: BorderRadius.circular(0),
                          ),
                        ),
                        onPressed: () {
                          controller.text += "+";
                        }, // Gọi hàm delete khi bấm nút này
                        child: const Center(child: Text(" + "))),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(4),
                child: NumberButton(
                  number: 4,
                  controller: controller,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 2, 2, 2),
                child: NumberButton(
                  number: 5,
                  controller: controller,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(2, 2, 2, 2),
                child: NumberButton(
                  number: 6,
                  controller: controller,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(2),
                child: SizedBox(
                  height: 56,
                  width: 56,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                            color: Color.fromRGBO(9, 173, 170, 1), width: 0.2),
                        borderRadius: BorderRadius.circular(0),
                      ),
                    ),
                    onPressed: () {
                      if (controller.text.isNotEmpty) {
                        controller.text = controller.text
                            .substring(0, controller.text.length - 1);
                      }
                    }, // Gọi hàm delete khi bấm nút này
                    child: const Center(child: Icon(Icons.arrow_back)),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 4, 6, 4),
                  child: SizedBox(
                    height: 56,
                    width: 56,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: const BorderSide(
                                color: Color.fromRGBO(9, 173, 170, 1),
                                width: 0.2),
                            borderRadius: BorderRadius.circular(0),
                          ),
                        ),
                        onPressed: () {
                          controller.text += "-";
                        }, // Gọi hàm delete khi bấm nút này
                        child: const Center(child: Text(" - "))),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  NumberButton(
                    number: 7,
                    controller: controller,
                  ),
                  NumberButton(
                    number: 0,
                    controller: controller,
                  ),
                ],
              ),
              Column(
                children: [
                  NumberButton(
                    number: 8,
                    controller: controller,
                  ),
                  NumberDot(title: ".", controller: controller),
                ],
              ),
              Column(
                children: [
                  NumberButton(
                    number: 9,
                    controller: controller,
                  ),
                  NumberDot(title: "00", controller: controller),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(0),
                child: SizedBox(
                  height: 116,
                  width: 56,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                              color: Color.fromRGBO(9, 173, 170, 1),
                              width: 0.2),
                          borderRadius: BorderRadius.circular(0),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          equalPressed();
                        });
                      }, // Gọi hàm delete khi bấm nút này
                      child: const Center(child: Text("="))),
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: SizedBox(
                      height: 56,
                      width: 56,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: const BorderSide(
                                  color: Color.fromRGBO(9, 173, 170, 1),
                                  width: 0.2),
                              borderRadius: BorderRadius.circular(0),
                            ),
                          ),
                          onPressed: () {
                            controller.text += "x";
                          }, // Gọi hàm delete khi bấm nút này
                          child: const Center(child: Text("x"))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: SizedBox(
                      height: 56,
                      width: 56,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: const BorderSide(
                                  color: Color.fromRGBO(9, 173, 170, 1),
                                  width: 0.2),
                              borderRadius: BorderRadius.circular(0),
                            ),
                          ),
                          onPressed: () {
                            controller.text += "/";
                          }, // Gọi hàm delete khi bấm nút này
                          child: const Center(child: Text("/"))),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

// define NumberButton widget
// its shape is round
class NumberButton extends StatelessWidget {
  final int number;

  final TextEditingController controller;

  const NumberButton({
    Key? key,
    required this.number,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2),
      child: SizedBox(
        height: 56,
        width: 56,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              side: const BorderSide(
                  color: Color.fromRGBO(9, 173, 170, 1), width: 0.2),
              borderRadius: BorderRadius.circular(0),
            ),
          ),
          onPressed: () {
            controller.text += number
                .toString(); // controller trỏ vào element của widget sẽ tự động được cộng thêm giá trị của số vào sau khi nhấn
          },
          child: Center(
            child: Text(
              number.toString(),
              style: const TextStyle(color: Colors.black, fontSize: 12),
            ),
          ),
        ),
      ),
    );
  }
}

// Phím dấu .
class NumberDot extends StatefulWidget {
  final String title;
  final TextEditingController controller;
  NumberDot({
    super.key,
    required this.title,
    required this.controller,
  });

  @override
  State<NumberDot> createState() => _NumberDotState();
}

class _NumberDotState extends State<NumberDot> {
  bool hasDot = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: SizedBox(
          height: 56,
          width: 56,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                side: const BorderSide(
                    color: Color.fromRGBO(9, 173, 170, 1), width: 0.2),
                borderRadius: BorderRadius.circular(0),
              ),
            ),
            onPressed: () {
              if (widget.title == ".") {
                if (hasDot == false) {
                  setState(() {
                    hasDot = true;
                  });
                  widget.controller.text += widget.title;
                } else {}
              } else {
                widget.controller.text += widget.title;
              }
            },
            child: Center(
              child: Text(
                widget.title,
                style: const TextStyle(color: Colors.black, fontSize: 15),
              ),
            ),
          )),
    );
  }
}
