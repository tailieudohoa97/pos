import 'package:flutter/material.dart'; //File nào cũng phải có
import 'package:intl/intl.dart'; //Format số thập phân - NumberFormat.currency
import './AddNewProduct.dart';

class EmptyContent extends StatelessWidget {
  const EmptyContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Text(
          'Featured is empty',
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }
}

class FeaturedProductsTabContent extends StatelessWidget {
  const FeaturedProductsTabContent({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 3,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio:
            1.5 / 1, //là tỉ lệ chiều rộng / chiều cao của mỗi sản phẩm
        // padding: const EdgeInsets.all(10),
        children: [
          ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return const AlertDialog(
                    content: AddNewProduct(),
                    contentPadding: EdgeInsets.zero,
                  );
                },
              );
            },
            style: TextButton.styleFrom(
              elevation: 0,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              foregroundColor: Colors.white,
              backgroundColor: Colors.grey[300],
            ),
            child: const Icon(
              Icons.add,
              color: Colors.white,
              size: 60,
            ),
          ),
          const ProductItem(
            imagePath: 'sp1.png',
            name: 'Apple Pie',
            price: 4.50,
            stock: 50,
          ),
          const ProductItem(
            imagePath: 'sp2.png',
            name: 'Blueberry Cupcake',
            price: 3.40,
            stock: 30,
          ),
          const ProductItem(
            imagePath: 'sp3.png',
            name: 'Chocolate Cupcake',
            price: 4.50,
            stock: 20,
          ),
          const ProductItem(
            imagePath: 'sp4.png',
            name: 'Orange Juice',
            price: 2.20,
            stock: 10,
          ),
          const ProductItem(
            imagePath: 'sp5.png',
            name: 'Muffin',
            price: 4.00,
            stock: 50,
          ),
          const ProductItem(
            imagePath: 'sp6.png',
            name: 'Chocolate',
            price: 5.50,
            stock: 50,
          ),
        ],
      ),
    );
  }
}

//PRODUC ITEM LAYOUT
class ProductItem extends StatelessWidget {
  final String imagePath;
  final String name;
  final double price;
  final int stock;

  const ProductItem({
    required this.imagePath,
    required this.name,
    required this.price,
    required this.stock,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final priceFormatter = NumberFormat.currency(
      symbol: '\$',
      decimalDigits: 2,
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
      ),
      clipBehavior: Clip.hardEdge,
      child: Stack(
        children: [
          Image.asset(
            imagePath,
            fit: BoxFit.cover,
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
              padding: const EdgeInsets.all(5.0),
              decoration: const BoxDecoration(color: Colors.black54),
              child: Wrap(
                alignment: WrapAlignment.spaceBetween,
                children: [
                  Text(
                    name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    priceFormatter.format(price),
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: Container(
              padding: const EdgeInsets.all(5.0),
              decoration: const BoxDecoration(color: Color(0xFFa0a700)),
              child: Text(
                '$stock in stock',
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                  fontSize: 12,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
