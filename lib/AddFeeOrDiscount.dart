import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './TextStyle.dart';

import 'Numpad.dart';
import 'SubNumpad.dart';

class AddFeeOrDiscount extends StatefulWidget {
  final double orderAmount;
  const AddFeeOrDiscount({super.key, required this.orderAmount});

  @override
  State<AddFeeOrDiscount> createState() => _AddFeeOrDiscountState();
}

class _AddFeeOrDiscountState extends State<AddFeeOrDiscount> {
  final TextEditingController _myController = TextEditingController();
  double amount = 0;
  double amountTotal = 0;
  String _FoDMethod = "1";
  String _type = "1";
  void initState() {
    super.initState();
    _myController.addListener(_onTextChanged);
  }

  void _onTextChanged() {
    // Handle the text change event here
    setState(() {
      if (_myController.text.isEmpty) {
        amount = 0;
      } else {
        amount = double.parse(_myController.text);
      }
      //print(ShippingFee);
    });
  }

  double CheckAmount(double orderAmount, double amount) {
    if (_FoDMethod == "1") {
      if (_type == "1") {
        return amount < orderAmount ? orderAmount - amount : 0;
      } else {
        return orderAmount - orderAmount * amount / 100;
      }
    } else {
      if (_type == "1") {
        return orderAmount + amount;
      } else {
        return orderAmount + orderAmount * amount / 100;
      }
    }
  }

  @override
  void dispose() {
    _myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
        width: 540,
        child: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Close button
              Container(
                alignment: Alignment.topRight,
                child: CloseButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),

              //Title
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                padding: const EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.black.withOpacity(0.2),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Add fee or discount to order total",
                      style: TextStyleGlobal.titlePopupBlackTextStyle,
                    ),
                    Text(
                      "\$${widget.orderAmount}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(9, 173, 170, 1),
                          fontSize: 18),
                    ),
                  ],
                ),
              ),

              //Row 1
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: FoDMethod(handleOnChange: (value) {
                        setState(() {
                          _FoDMethod = value;
                        });
                      }),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: AmountType(
                          handleOnChange: (value) {
                            setState(() {
                              _type = value;
                            });
                          },
                          type: _type),
                    ),
                  ),
                  Expanded(
                    child: AmountTextField(
                      controller: _myController,
                    ),
                  )
                ],
              ),

              //Row 2
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: const ReasonTextField()),
                  ),
                  Expanded(
                    flex: 1,
                    child: AmountToPay(
                      amountToPay: double.parse(
                          CheckAmount(widget.orderAmount, amount)
                              .toStringAsFixed(2)),
                    ),
                  )
                ],
              ),

              //Popular fees
              const PopularFees(),

              //Popular Percent
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      child: Percentage(
                        data: "5",
                        handleOnChange: (value) {
                          setState(() {
                            _type = "2";
                            _myController.text = "5";
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      child: Percentage(
                        data: "10",
                        handleOnChange: (value) {
                          setState(() {
                            _type = "2";
                            _myController.text = "10";
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      child: Percentage(
                        data: "15",
                        handleOnChange: (value) {
                          setState(() {
                            _type = "2";
                            _myController.text = "15";
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      child: Percentage(
                        data: "20",
                        handleOnChange: (value) {
                          setState(() {
                            _type = "2";
                            _myController.text = "20";
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Percentage(
                        data: "50",
                        handleOnChange: (value) {
                          setState(() {
                            _type = "2";
                            _myController.text = "50";
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),

              NumPad(
                submitText: "UPDATE TOTAL",
                buttonSize: 75,
                buttonColor: Colors.white,
                iconColor: const Color.fromRGBO(9, 173, 170, 1),
                controller: _myController,
                delete: () {
                  if (_myController.text.length >= 1) {
                    _myController.text = _myController.text
                        .substring(0, _myController.text.length - 1);
                  }
                },
                clear: () {
                  _myController.text = "";
                },
                // do something with the input numbers
                onSubmit: () {
                  debugPrint('Your code: ${_myController.text}');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FoDMethod extends StatefulWidget {
  final Function handleOnChange;
  const FoDMethod({super.key, required this.handleOnChange});

  @override
  State<FoDMethod> createState() => _FoDMethodState();
}

class _FoDMethodState extends State<FoDMethod> {
  String? FoDMethod;

  List<Map> get _myJson => [
        {
          'id': '1',
          'icon': Icons.trending_down,
          'name': 'Discount',
        },
        {
          'id': '2',
          'icon': Icons.trending_up,
          'name': 'Fee',
        },
      ];

  @override
  void initState() {
    super.initState();
    FoDMethod = _myJson[0]['id'].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Add fee or discount",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: const Text('Select...'),

                              value: FoDMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: FoDMethod," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                widget.handleOnChange(newValue);
                                setState(() {
                                  FoDMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: _myJson.map(
                                (Map map) {
                                  return DropdownMenuItem(
                                    value: map['id'].toString(),
                                    child: Row(
                                      children: [
                                        Icon(map['icon']),
                                        const SizedBox(width: 10),
                                        Text(map['name']),
                                      ],
                                    ),
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

//[022324HG] Add icon into list seclect
// class AmountType extends StatelessWidget {
//   final Function handleOnChange;
//   final String type;
//   const AmountType(
//       {super.key, required this.handleOnChange, required this.type});

//   @override
//   Widget build(BuildContext context) {
//     List listItem = [
//       'Fixed',
//       'Percentage',
//     ];
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         const Padding(
//           padding: EdgeInsets.only(top: 20, bottom: 10),
//           child: Text(
//             "Amount type",
//             style: TextStyleGlobal.regBlackTextStyle,
//           ),
//         ),
//         Container(
//           height: 40,
//           decoration: BoxDecoration(
//             border: Border.all(
//               width: 1,
//               color: const Color(0xFFdbdbdb),
//             ),
//             borderRadius: BorderRadius.circular(5),
//           ),
//           child: Row(
//             children: [
//               Expanded(
//                 child: Stack(
//                   children: [
//                     Row(
//                       children: [
//                         Expanded(
//                           child: ButtonTheme(
//                             alignedDropdown: true,
//                             child: DropdownButton(
//                               //Ban đầu rỗng, sẽ hiển thị cái hint
//                               hint: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: const [
//                                   Icon(Icons.qr_code_scanner),
//                                   SizedBox(width: 5),
//                                   Text('Fixed'),
//                                 ],
//                               ),
//                               value: type,
//                               isExpanded: true,
//                               underline: const SizedBox(),
//                               style: const TextStyle(
//                                 fontSize: 13,
//                                 color: Colors.black,
//                               ),
//                               icon: const Icon(Icons.keyboard_arrow_down),
//                               // Mỗi lần click chọn 1 item
//                               // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
//                               // dẫn tới "value: chooseStore," ở phía trên thay đổi
//                               // Và cái hint sẽ bị thay = cái string item đã chọn
//                               onChanged: (newValue) {
//                                 handleOnChange(newValue);
//                               },

//                               //Tạo list item, layout cho 1 item
//                               items: listItem.map((newValue) {
//                                 return DropdownMenuItem(
//                                   // alignment: Alignment.centerLeft,
//                                   value: newValue,
//                                   child: Text(newValue),
//                                 );
//                               }).toList(),
//                             ),
//                           ),
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }

class AmountType extends StatefulWidget {
  final Function handleOnChange;
  final String type;
  const AmountType(
      {super.key, required this.handleOnChange, required this.type});

  @override
  State<AmountType> createState() => _AmountTypeState();
}

class _AmountTypeState extends State<AmountType> {
  List<Map> get _myJson => [
        {
          'id': '1',
          'icon': '\$',
          'name': 'Fixed',
        },
        {
          'id': '2',
          'icon': '%',
          'name': 'Percentage',
        },
      ];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Amount type",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: const Text('Choose....'),

                              value: widget.type,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: type," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                //print(newValue);
                                // handleOnChange(newValue);
                                widget.handleOnChange(newValue);
                              },

                              //Tạo list item, layout cho 1 item
                              items: _myJson.map(
                                (Map map) {
                                  return DropdownMenuItem(
                                    value: map['id'].toString(),
                                    child: Row(
                                      children: [
                                        Text(map['icon']),
                                        const SizedBox(width: 10),
                                        Text(map['name']),
                                      ],
                                    ),
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class AmountTextField extends StatelessWidget {
  final TextEditingController controller;
  const AmountTextField({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Amount *",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
            ],
            controller: controller,
            textAlign: TextAlign.right,
            decoration: const InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFF09ADAA))),
              hintText: "0", // hiển thị hint = "0"
              hintStyle: TextStyle(color: Colors.black),
            ),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class ReasonTextField extends StatelessWidget {
  const ReasonTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Reason (optional)",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        SizedBox(
          height: 40,
          child: TextField(
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Color(0xFF09ADAA))),
            ),
            cursorColor: Colors.black,
          ),
        ),
      ],
    );
  }
}

class AmountToPay extends StatelessWidget {
  final double amountToPay;
  const AmountToPay({super.key, required this.amountToPay});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Amount to pay",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                height: 40,
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Color(0xFFdbdbdb),
                      style: BorderStyle.solid,
                    ),
                  ),
                ),
                alignment: Alignment.centerRight,
                child: Text(
                  "\$$amountToPay",
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF09ADAA),
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class PopularFees extends StatelessWidget {
  const PopularFees({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Popular fees",
            style: TextStyleGlobal.regBlackTextStyle,
          ),
        ),
      ],
    );
  }
}

class Percentage extends StatefulWidget {
  final String data;
  final Function handleOnChange;
  const Percentage({
    Key? key,
    required this.data,
    required this.handleOnChange,
  }) : super(key: key);

  @override
  State<Percentage> createState() => _PercentageState();
}

class _PercentageState extends State<Percentage> {
  // Color _color = Colors.grey;
  bool isHover = false;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      // width: 100,
      child: ElevatedButton(
        onHover: (event) => {
          setState(
            () {
              isHover = !isHover;
            },
          )
        },
        onPressed: () => widget.handleOnChange(widget.data),
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color:
                  isHover ? Color.fromRGBO(9, 173, 170, 1) : Color(0xFFdbdbdb),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        child: Text(
          "${widget.data}%",
          style: TextStyle(
              fontWeight: isHover ? FontWeight.bold : FontWeight.normal,
              color: isHover ? Color(0xFF09ADAA) : Colors.grey),
        ),
      ),
    );
  }
}
