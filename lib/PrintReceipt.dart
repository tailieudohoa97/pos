import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:my_pos_122219/AllProductsTabContent.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

void Receipt() async {
  final doc = pw.Document();

  /// for using an image from assets
  final image = await imageFromAssetBundle('assets/logo-pos.png');

  doc.addPage(
    pw.Page(
      pageFormat: PdfPageFormat.a4,
      build: (pw.Context context) {
        return pw.Center(
            child: pw.Container(
          child: pw.Center(
            child: pw.SizedBox(
              width: 700,
              child: pw.Column(children: [
                pw.ConstrainedBox(
                  constraints: const pw.BoxConstraints(
                    maxWidth: 320,
                    maxHeight: 40,
                  ),
                  child: pw.Image(image),
                ),
                pw.Text(
                  "Food Store",
                  style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold, fontSize: 20),
                ),
                pw.Text("VAT:" + " 654342165444"),
                pw.Text("East Tailwood Ave."),
                pw.Text("South Richmond Hill, NY 9019"),
                pw.Text("Phone: " + "88888888"),
                pw.SizedBox(height: 25),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: pw.CrossAxisAlignment.center,
                  children: [
                    // Row Left
                    pw.Row(
                      children: [
                        pw.Text(
                          "1" + " x ",
                          style: pw.TextStyle(
                            fontSize: 15,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        ),
                        pw.Text(
                          "Apple Pie",
                          style: pw.TextStyle(
                            fontSize: 15,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                      ],
                    ),

                    // Row right
                    pw.Row(
                      children: [
                        pw.ConstrainedBox(
                          constraints: const pw.BoxConstraints(
                            minWidth: 270,
                          ),
                          child: pw.Row(
                            mainAxisAlignment: pw.MainAxisAlignment.end,
                            crossAxisAlignment: pw.CrossAxisAlignment.center,
                            children: [
                              pw.Text("1 x ",
                                  style: pw.TextStyle(
                                    fontSize: 14,
                                    fontWeight: pw.FontWeight.bold,
                                  )),
                              pw.Text(
                                ('\$123'),
                                style: pw.TextStyle(
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        pw.ConstrainedBox(
                          constraints: const pw.BoxConstraints(
                            minWidth: 100,
                          ),
                          child: pw.Row(
                            mainAxisAlignment: pw.MainAxisAlignment.end,
                            crossAxisAlignment: pw.CrossAxisAlignment.center,
                            children: [
                              pw.Text(
                                ('\$231'),
                                style: pw.TextStyle(
                                  fontSize: 18,
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                pw.SizedBox(height: 25),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: [
                    pw.Text("Total:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 20)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("\$7.90",
                          style: const pw.TextStyle(fontSize: 17)),
                    )
                  ],
                ),
                pw.SizedBox(height: 10),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: [
                    pw.Text("Cash:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("\$7.90",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.SizedBox(height: 10),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: [
                    pw.Text("Change:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("\$7.90",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.SizedBox(height: 25),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Date:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text(DateTime.now().toString(),
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Order:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("233668",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Customer:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("Guest",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Register:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("Register 1",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Cashier:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("Test Test",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.SizedBox(height: 30),
                pw.Text(
                  "Thanks for your purchase",
                  style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold, fontSize: 20),
                )
              ]),
            ),
          ),
        )); // Center
      },
    ),
  ); // Page

  await Printing.layoutPdf(
      onLayout: (PdfPageFormat format) async => doc.save());
}

void PrintGiftReceipt() async {
  final doc = pw.Document();

  /// for using an image from assets
  final image = await imageFromAssetBundle('assets/logo-pos.png');

  doc.addPage(
    pw.Page(
      pageFormat: PdfPageFormat.a4,
      build: (pw.Context context) {
        return pw.Center(
            child: pw.Container(
          child: pw.Center(
            child: pw.SizedBox(
              width: 700,
              child: pw.Column(children: [
                pw.ConstrainedBox(
                  constraints: const pw.BoxConstraints(
                    maxWidth: 320,
                    maxHeight: 40,
                  ),
                  child: pw.Image(image),
                ),
                pw.Text(
                  "Food Store",
                  style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold, fontSize: 20),
                ),
                pw.Text("VAT:" + " 654342165444"),
                pw.Text("East Tailwood Ave."),
                pw.Text("South Richmond Hill, NY 9019"),
                pw.Text("Phone: " + "88888888"),
                pw.SizedBox(height: 25),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: pw.CrossAxisAlignment.center,
                  children: [
                    // Row Left
                    pw.Row(
                      children: [
                        pw.Text(
                          "1" + " x ",
                          style: pw.TextStyle(
                            fontSize: 15,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        ),
                        pw.Text(
                          "Apple Pie",
                          style: pw.TextStyle(
                            fontSize: 15,
                            fontWeight: pw.FontWeight.bold,
                          ),
                        )
                      ],
                    ),

                    // Row right
                  ],
                ),
                pw.SizedBox(height: 30),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Date:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text(DateTime.now().toString(),
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Order:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("233668",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Customer:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("Guest",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Register:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("Register 1",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.start,
                  children: [
                    pw.Text("Cashier:",
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 13)),
                    pw.Padding(
                      padding: const pw.EdgeInsets.symmetric(horizontal: 15),
                      child: pw.Text("Test Test",
                          style: const pw.TextStyle(fontSize: 13)),
                    )
                  ],
                ),
                pw.SizedBox(height: 30),
                pw.Text(
                  "Thanks for your purchase",
                  style: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold, fontSize: 20),
                )
              ]),
            ),
          ),
        )); // Center
      },
    ),
  ); // Page
  await Printing.layoutPdf(
      onLayout: (PdfPageFormat format) async => doc.save());
}

class PrintReceipt extends StatelessWidget {
  const PrintReceipt({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          OrderDetail(
            customer: 'Guest',
            datetime: DateTime.now(),
            orderId: '23244',
            paymentOption: 'Cash',
            status: 'Completed',
          ),
          const LayoutProductInCart(
            image: 'sp1.png',
            name: 'Apple Pie',
            price: 5.50,
            total: 5.50,
          ),
          const LayoutProductInCart(
            image: 'sp2.png',
            name: 'Blueberry Cupcake',
            price: 6.50,
            total: 6.50,
          ),
          const LayoutProductInCart(
            image: 'sp3.png',
            name: 'Chocolate Cupcake',
            price: 7.00,
            total: 7.00,
          ),
          const TotalSales(),
          const ProfitItem(
            data: '\$123.3',
            title: 'Cash',
          ),
          const ProfitItem(
            data: '\$234.2',
            title: 'Changes',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: () {
                    Receipt();
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(67, 87, 86, 1),
                    shape: RoundedRectangleBorder(
                      side: const BorderSide(
                          color: Color.fromRGBO(67, 87, 86, 1), width: 0.2),
                      borderRadius: BorderRadius.circular(0),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'Print Receipt'.toUpperCase(),
                      style: const TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    PrintGiftReceipt();
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(54, 47, 97, 1),
                    shape: RoundedRectangleBorder(
                      side: const BorderSide(
                          color: Color.fromRGBO(54, 47, 97, 1), width: 0.2),
                      borderRadius: BorderRadius.circular(0),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'Print Gift Receipt'.toUpperCase(),
                      style: const TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }
}

class OrderDetail extends StatelessWidget {
  final String orderId;
  final String paymentOption;
  final DateTime datetime;
  final String status;
  final String customer;

  const OrderDetail(
      {super.key,
      required this.orderId,
      required this.paymentOption,
      required this.datetime,
      required this.status,
      required this.customer});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            color: const Color.fromRGBO(9, 173, 170, 1),
            child: Padding(
              padding: const EdgeInsets.all(19),
              child: Text("Order #$orderId"),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              color: const Color.fromRGBO(219, 219, 219, 1),
              alignment: const Alignment(-1, 1),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text("Paid via $paymentOption on $datetime - "),
                        Text(
                          status,
                          style: const TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        const Text("Customer: "),
                        Text(
                          customer,
                          style: const TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class LayoutProductInCart extends StatelessWidget {
  // const LayoutProductInCart({super.key});
  final String image;
  final String name;
  final double price;
  final double total;
  const LayoutProductInCart({
    Key? key,
    required this.image,
    required this.name,
    required this.price,
    required this.total,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Color(0xFFdbdbdb),
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Row Left
            Row(
              children: [
                Image.asset(
                  image,
                  width: 100,
                  fit: BoxFit.cover,
                ),
                const SizedBox(width: 25),
                Text(
                  name,
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),

            // Row right
            Row(
              children: [
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minWidth: 270,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text("1 x ",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF888888),
                          )),
                      Text(
                        ('\$$price'),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFF888888),
                        ),
                      ),
                    ],
                  ),
                ),
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minWidth: 100,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        ('\$$total'),
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class TotalSales extends StatelessWidget {
  const TotalSales({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Text("Total Sales",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(9, 173, 170, 1),
                  fontSize: 18)),
          Text("\$720.34",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromRGBO(9, 173, 170, 1),
                  fontSize: 18))
        ],
      ),
    );
  }
}

class ProfitItem extends StatelessWidget {
  final String title;
  final String data;
  const ProfitItem({super.key, required this.title, required this.data});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15)),
          Text(data, style: const TextStyle(color: Colors.black, fontSize: 15))
        ],
      ),
    );
  }
}

class Information extends StatelessWidget {
  final String title;
  final String data;
  const Information({super.key, required this.title, required this.data});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(title + ":",
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 15)),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Text(data,
                style: const TextStyle(color: Colors.black, fontSize: 15)),
          )
        ],
      ),
    );
  }
}
