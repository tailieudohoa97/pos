import 'dart:ui';
import 'dart:html' as html;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart'; //Sử dụng cho nhập liệu TextField - FilteringTextInputFormatter
import 'package:my_pos_122219/AddCoupon.dart';
import 'package:my_pos_122219/AddFeeOrDiscount.dart';
import 'package:my_pos_122219/AddNote.dart';
import 'package:my_pos_122219/Payment.dart';
import 'package:my_pos_122219/Shipping.dart';
import 'package:my_pos_122219/SubNumpad.dart';
import 'package:my_pos_122219/SuspendAndSaveCart.dart';

//Header
import './header.dart';

// MainLeft
import './AddNewProduct.dart';
import './AllProductsTabContent.dart';
import './OnSaleTabContent.dart';
import './FeaturedProductsTabContent.dart';

// MainRight
import './CartTabContent.dart';
import './CustomerTabContent.dart';
import './AddShippingAddress.dart';
import './SaveCartTabContent.dart';
import './AddNote.dart';
import './AddFeeOrDiscount.dart';
import './AddCoupon.dart';
import './Shipping.dart';
import './SuspendAndSaveCart.dart';
import './Payment.dart';

void disableBrowserBackButton() {
  html.window.history.pushState(null, "", html.window.location.href);
  html.window.onPopState.listen((event) {
    html.window.history.pushState(null, "", html.window.location.href);
  });
}

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    disableBrowserBackButton();
    return Scaffold(
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              const Header(),
              Row(
                children: const [
                  MainLeft(),
                  MainRight(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MainLeft extends StatelessWidget {
  const MainLeft({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(234, 234, 234, 1),
      height: MediaQuery.of(context).size.height - 50,
      width: MediaQuery.of(context).size.width / 2,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            RegisterNavbarLeft(),
            ContentLeft(),
          ],
        ),
      ),
    );
  }
}

class MainRight extends StatelessWidget {
  const MainRight({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          left: BorderSide(
            color: Color(0xFFeaeaea),
            width: 1,
          ),
        ),
        color: Color.fromRGBO(234, 234, 234, 1),
      ),
      height: MediaQuery.of(context).size.height - 50,
      width: MediaQuery.of(context).size.width / 2,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            RegisterNavbarRight(),
            ContentRight(),
          ],
        ),
      ),
    );
  }
}

class RegisterNavbarLeft extends StatelessWidget {
  const RegisterNavbarLeft({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          //Logo
          ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 120,
              maxHeight: 40,
            ),
            child: Image.asset('logo-pos.png'),
          ),

          const SizedBox(width: 10),

          //Search Bar
          Expanded(
            child: Container(
              height: 35,
              color: Colors.white,
              child: const TextField(
                decoration: InputDecoration(
                  hintText: 'Search Product...',
                  hintStyle: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w100,
                  ),
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromRGBO(9, 173, 170, 1),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromARGB(255, 173, 148, 9),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  border: OutlineInputBorder(),
                ),
                // clipBehavior: Clip.hardEdge,
                cursorColor: Colors.black,
                cursorHeight: 20,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 12,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),

          //Calculater icon
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(content: SubNumPad());
                },
              );
              ;
            },
            icon: const Icon(
              Icons.calculate,
              color: Color(0xFF435756),
            ),
            iconSize: 50,
            tooltip: 'Open calculator',
            mouseCursor: SystemMouseCursors.click,
          ),
        ],
      ),
    );
  }
}

class ContentLeft extends StatelessWidget {
  const ContentLeft({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5, //Số tab có thể Click được
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: const Color(0xFF09adaa),
                border: Border.all(
                  width: 0.0,
                  color: const Color(0xFFeaeaea),
                ),
              ),

              //TabBar
              child: TabBar(
                //padding: const EdgeInsets.all(0),
                isScrollable: true, //Co giãn theo độ dài ký tự và Scroll

                //Selecting
                labelColor: const Color(0xFF000000),
                labelStyle: const TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                ),
                labelPadding: const EdgeInsets.symmetric(horizontal: 0),

                indicatorWeight: 0,
                //indicatorPadding: const EdgeInsets.all(0),
                //indicatorColor: Colors.yellow,
                indicatorSize: TabBarIndicatorSize.tab,
                indicator: const BoxDecoration(
                  color: Color(0xFFeaeaea), //Change background color from here
                ),

                //UnSelect
                unselectedLabelColor: const Color(0xFFFFFFFF),

                //Tabs
                tabs: [
                  const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Tab(text: 'All')),
                  const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Tab(text: 'On Sale')),
                  const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Tab(text: 'Featured')),
                  Tab(
                    child: SizedBox(
                      height: 70,
                      child: ElevatedButton(
                        onPressed: (() {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return const AlertDialog(
                                  content: AddNewProduct(),
                                  contentPadding: EdgeInsets.zero,
                                );
                              });
                        }),
                        style: TextButton.styleFrom(
                          elevation: 0,
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          foregroundColor: Colors.white,
                          backgroundColor:
                              const Color.fromARGB(150, 8, 157, 155),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(Icons.add, color: Colors.white, size: 20),
                            SizedBox(width: 5),
                            Text(
                              'Add product',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      height: 70,
                      child: ElevatedButton(
                        onPressed: null,
                        style: TextButton.styleFrom(
                          elevation: 0,
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          foregroundColor: Colors.white,
                          backgroundColor:
                              const Color.fromARGB(150, 6, 122, 120),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.qr_code_scanner,
                              color: Colors.white,
                              size: 20,
                            ),
                            SizedBox(width: 5),
                            Text(
                              'Scan product',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),

            //Contents of each Tab
            Container(
              // width: screenWidth,
              padding: const EdgeInsets.all(20),
              height: MediaQuery.of(context).size.height - 186,
              child: TabBarView(
                children: [
                  //ALL products
                  const AllProductsTabContent(),

                  //ON SALE PRO
                  const OnSaleTabContent(),

                  //FEATURED PRO
                  const FeaturedProductsTabContent(),

                  //ADD PRODUCT
                  //This content will be displayed when closing the showDialog in the OnPressed of the Button
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.add,
                        size: 40,
                      ),
                      SizedBox(width: 5),
                      Text(
                        'Add product',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),

                  //SCAN PRO
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.qr_code_scanner,
                        size: 40,
                      ),
                      SizedBox(width: 5),
                      Text(
                        'Scan Product',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class RegisterNavbarRight extends StatelessWidget {
  const RegisterNavbarRight({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Tabs Menu
            Container(
              height: 55,
              decoration: const BoxDecoration(color: Color(0xFFEAEAEA)),

              //TabBar
              child: TabBar(
                isScrollable: true,

                //Selecting
                labelColor: Colors.black,
                labelStyle: const TextStyle(fontSize: 13),
                labelPadding: const EdgeInsets.symmetric(horizontal: 0),

                indicatorWeight: 0,
                indicatorColor: const Color(0xFFeaeaea),
                indicator: const BoxDecoration(
                  color: Colors.white,
                ),
                indicatorSize: TabBarIndicatorSize.tab,

                //UnSelect
                unselectedLabelColor: const Color(0xFF7A7A7A),

                //TABS
                tabs: [
                  //CART
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.shopping_cart_sharp,
                          size: 18,
                        ),
                        const SizedBox(width: 5),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Text('Cart'),
                          ],
                        ),
                      ],
                    ),
                  ),

                  //CUSTOMER
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.face,
                          size: 18,
                        ),
                        const SizedBox(width: 5),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Text(
                              'Customer',
                              style: TextStyle(
                                fontSize: 13,
                              ),
                            ),
                            Text(
                              'Guest',
                              style: TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),

                  //ADDRESS
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.location_pin,
                          size: 18,
                        ),
                        const SizedBox(width: 5),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Text(
                              'Address',
                              style: TextStyle(
                                fontSize: 13,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),

                  //SAVE CART
                  Container(
                    color: const Color(0xC9FFA826),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.save,
                          size: 30,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            //Contents of each Tab
            Container(
              // padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              color: const Color(0xFFFFFFFF),
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                  minHeight: 250,
                ),
                child: SizedBox(
                  //Header: 55, Menu tabs: 50, Total and list buttons: 257 ~~ 260
                  height: MediaQuery.of(context).size.height - 55 - 50 - 260,
                  // height: MediaQuery.of(context).size.height - 405,
                  child: const TabBarView(
                    children: [
                      //CART'S CONTENT
                      CartTabContent(),

                      //CUSTOMER'S CONTENT
                      CustomerTabContent(),

                      //ADDRESS'S CONTENT
                      AddShippingAddress(),

                      //SAVE CART'S CONTENT
                      SaveCartTabContent(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

//Total and list buttons
class ContentRight extends StatelessWidget {
  const ContentRight({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFFFFFFFF),
      padding: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 7),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "SUBtotal".toUpperCase(),
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 13),
                ),
                const Text(
                  "\$0.00",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 13),
                )
              ],
            ),
          ),
          Divider(
            // height: 40,
            thickness: 4,
            color: Colors.black.withOpacity(0.1),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text(
                  "TOTAL",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(9, 173, 170, 1),
                      fontSize: 18),
                ),
                Text(
                  "\$0.00",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(9, 173, 170, 1),
                      fontSize: 18),
                )
              ],
            ),
          ),
          const ListButton(),
        ],
      ),
    );
  }
}

class ListButton extends StatefulWidget {
  const ListButton({super.key});

  @override
  State<ListButton> createState() => _ListButtonState();
}

class _ListButtonState extends State<ListButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            // The space to balance one row
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                // decoration: BoxDecoration(color: Colors.grey),
                padding: const EdgeInsets.all(10),
                child: const Text(''),
              ),
            ),

            // The space to balance one row
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                // decoration: BoxDecoration(color: Colors.grey),
                padding: const EdgeInsets.all(10),
                child: const Text(''),
              ),
            ),

            //ADD NOTE button
            Expanded(
              flex: 2,
              child: Container(
                height: 35,
                margin: const EdgeInsets.only(right: 10),
                child: TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: AddNote(
                            order_total: 100,
                          ),
                        );
                      },
                    );
                  },
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFF4d4d4d),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.message,
                        size: 16,
                      ),
                      const SizedBox(width: 5),
                      Text('ADD Note'.toUpperCase())
                    ],
                  ),
                ),
              ),
            ),

            //EMPTY CART button
            Expanded(
              flex: 2,
              child: SizedBox(
                height: 35,
                child: TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFFc65338),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.close,
                        size: 16,
                      ),
                      const SizedBox(width: 5),
                      Text('Empty Cart'.toUpperCase())
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(height: 5),
        Row(
          children: [
            //ADD FEE OR DISCOUNT button
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                height: 100,
                width: 115,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return const AlertDialog(
                            content: AddFeeOrDiscount(
                          orderAmount: 20,
                        ));
                      },
                    );
                  },
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color.fromRGBO(9, 173, 170, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.add),
                      const SizedBox(height: 10),
                      Center(
                        child: Text(
                          "ADD FEE OR DISCOUNT".toUpperCase(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            //APPLY COUPON button
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                height: 100,
                width: 115,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return const AlertDialog(
                              content: AddCoupon(
                            orderAmount: 20,
                          ));
                        });
                  },
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color.fromRGBO(9, 173, 170, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.card_giftcard),
                      const SizedBox(height: 10),
                      Text(
                        "APPLY COUPON".toUpperCase(),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
            ),

            //SHIPPING button
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                height: 100,
                width: 115,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return const AlertDialog(
                            content: Shipping(
                          orderAmount: 20.00,
                        ));
                      },
                    );
                  },
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color.fromRGBO(9, 173, 170, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.local_shipping),
                      const SizedBox(height: 10),
                      Text(
                        "SHIPPING".toUpperCase(),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ),

            //SUSPEND  & SAVE CART button
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                height: 100,
                width: 115,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return const AlertDialog(content: SuspendAndSaveCart());
                      },
                    );
                  },
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFFe09914),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.save_as_rounded),
                      const SizedBox(height: 10),
                      Text(
                        "SUSPEND  & SAVE CART".toUpperCase(),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ),

            //PAY button
            Expanded(
              flex: 2,
              child: SizedBox(
                height: 100,
                width: 250,
                child: TextButton(
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFFa0a700),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    textStyle: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return const AlertDialog(
                            content: Payment(
                          orderAmount: 10.20,
                        ));
                      },
                    );
                  },
                  child: Text(
                    "PAY".toUpperCase(),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
