import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:my_pos_122219/AddCoupon.dart';
// import 'package:my_pos_122219/AddFeeOrDiscount.dart';
// import 'package:my_pos_122219/AddNewProduct.dart';
// import 'package:my_pos_122219/AddNote.dart';
// import 'package:my_pos_122219/AddShippingAddress.dart';
// import 'package:my_pos_122219/CloseRegister.dart';
// import 'package:my_pos_122219/ManageCash.dart';
import 'package:my_pos_122219/OrderHistory.dart';
// import 'package:my_pos_122219/Payment.dart';
import 'package:my_pos_122219/PrintReceipt.dart';
import 'package:my_pos_122219/RegisterMain.dart';
import 'package:my_pos_122219/SubNumpad.dart';
// import 'package:my_pos_122219/SuspendAndSaveCart.dart';
import 'package:my_pos_122219/TodayProfit.dart';

import './ScreenLogin.dart';
import './ScreenStart.dart';
// import './Shipping.dart';

void main() {
  runApp(
    MaterialApp(
      home: MyApp(
        widget: Center(child: ScreenLogin()),
      ),
      debugShowCheckedModeBanner: false,
    ),
  );
}

class SlidePageRoute<T> extends PageRouteBuilder<T> {
  final Widget child;

  SlidePageRoute({required this.child})
      : super(
          transitionDuration: Duration(milliseconds: 500),
          pageBuilder: (context, animation, secondaryAnimation) => child,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return Stack(
              children: [
                FadeTransition(
                  opacity: animation,
                  child: child,
                ),
                SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(1.0, 0.0),
                    end: Offset(0.0, 0.0),
                  ).animate(animation),
                  child: child,
                ),
              ],
            );
          },
        );
}

class MyApp extends StatelessWidget {
  final Widget widget;

  // const MyApp({Key? key}) : super(key: key);
  const MyApp({super.key, required this.widget});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'POS',
      theme: ThemeData(
        textTheme: GoogleFonts.openSansTextTheme(),
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: WillPopScope(
        onWillPop: () async {
          // SystemChrome.setEnabledSystemUIOverlays(
          //     [SystemUiOverlay.top, SystemUiOverlay.bottom]);
          // return false;
          SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
              overlays: [SystemUiOverlay.top, SystemUiOverlay.bottom]);
          return false;
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                widget,
              ],
            ),
          ),
        ),
      ),
      // Xóa nhãn debug trên màn hình ứng dụng, thường để là false
      // Delete "debug" label
      debugShowCheckedModeBanner: false,
    );
  }
}
