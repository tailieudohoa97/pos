import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'Numpad.dart';

class Shipping extends StatefulWidget {
  final double orderAmount;
  const Shipping({super.key, required this.orderAmount});

  @override
  State<Shipping> createState() => _ShippingState();
}

class _ShippingState extends State<Shipping> {
  final TextEditingController _myController = TextEditingController();
  void initState() {
    super.initState();
    _myController.addListener(_onTextChanged);
  }

  void _onTextChanged() {
    // Handle the text change event here
    setState(() {
      if (_myController.text.isEmpty) {
        ShippingFee = 0;
      } else {
        ShippingFee = double.parse(_myController.text);
      }
      //print(ShippingFee);
    });
  }

  @override
  void dispose() {
    _myController.dispose();
    super.dispose();
  }

  double ShippingFee = 0;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.black)),
        width: 540,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Align(
                  alignment: const Alignment(1, -1),
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.close))),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Shipping",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 18)),
                    Text("\$${widget.orderAmount}",
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(9, 173, 170, 1),
                            fontSize: 18))
                  ],
                ),
              ),
              Divider(
                height: 10,
                thickness: 2,
                color: Colors.black.withOpacity(0.2),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const ShippingMethod(),
                  AmountTextField(
                    controller: _myController,
                  )
                ],
              ),
              AmountToPay(amountToPay: widget.orderAmount + ShippingFee),
              NumPad(
                submitText: "SAVE SHIPPING",
                buttonSize: 75,
                buttonColor: Colors.white,
                iconColor: const Color.fromRGBO(9, 173, 170, 1),
                controller: _myController,
                delete: () {
                  if (_myController.text.isNotEmpty) {
                    _myController.text = _myController.text
                        .substring(0, _myController.text.length - 1);
                  }
                },
                clear: () {
                  _myController.text = "";
                },
                // do something with the input numbers
                onSubmit: () {
                  debugPrint('Your code: ${_myController.text}');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DetailCoupon extends StatelessWidget {
  const DetailCoupon({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 540,
      margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(color: Color.fromRGBO(240, 240, 240, 1)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          Text("Discount type: -",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 12,
                  height: 2)),
          Text("Coupon amount: -",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 12,
                  height: 2)),
          Text("Expiry date: -",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 12,
                  height: 2))
        ],
      ),
    );
  }
}

class AmountToPay extends StatelessWidget {
  final double amountToPay;
  const AmountToPay({super.key, required this.amountToPay});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      child: Column(
        children: [
          const Align(
            alignment: Alignment(-1, 1),
            child: Text("Amount to pay",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 12,
                    height: 2)),
          ),
          Align(
            alignment: Alignment(1, 1),
            child: Text("\$$amountToPay",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(9, 173, 170, 1),
                    fontSize: 18)),
          ),
          Divider(
            height: 10,
            thickness: 2,
            color: Colors.black.withOpacity(0.2),
          ),
        ],
      ),
    );
  }
}

class AmountTextField extends StatelessWidget {
  final TextEditingController controller;

  const AmountTextField({
    Key? key,
    required this.controller,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: SizedBox(
        width: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text("Amount",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 10)),
            ),
            TextField(
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
              ],
              controller: controller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(9, 173, 170, 1))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ShippingMethod extends StatefulWidget {
  const ShippingMethod({super.key});

  @override
  State<ShippingMethod> createState() => _ShippingMethodState();
}

class _ShippingMethodState extends State<ShippingMethod> {
  String shippingMethod = 'Flat rate';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = [
    'Flat rate',
    'Free shipping',
    'Local pickup',
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: Text("Shipping Method",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 10)),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
            width: 200,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                // color: const Color.fromRGBO(9, 173, 170, 1),
                color: const Color.fromRGBO(234, 234, 234, 1),
              ),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Icon(Icons.qr_code_scanner),
                                  SizedBox(width: 5),
                                  Text('Flat rate'),
                                ],
                              ),
                              value: shippingMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                setState(() {
                                  shippingMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
